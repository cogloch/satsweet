#include "imu.hpp"

#define BAIT 

#ifndef BAIT 
#include "SparkFunMPU9250-DMP.h"
#include "quaternionFilters.h"

#define SerialPort SerialUSB
MPU9250_DMP imu;

float lastUpdate = 0.f;
float deltat = 0.f;
#endif 

void IMU::Tick()
{
#ifndef BAIT 
    if (imu.dataReady())
    {
        imu.update(UPDATE_ACCEL | UPDATE_GYRO | UPDATE_COMPASS);
        
        float accelX = imu.calcAccel(imu.ax);
        float accelY = imu.calcAccel(imu.ay);
        float accelZ = imu.calcAccel(imu.az);
        float gyroX = imu.calcGyro(imu.gx);
        float gyroY = imu.calcGyro(imu.gy);
        float gyroZ = imu.calcGyro(imu.gz);
        float magX = imu.calcMag(imu.mx);
        float magY = imu.calcMag(imu.my);
        float magZ = imu.calcMag(imu.mz);

        float Now = micros();

        deltat = ((Now - lastUpdate) / 1000000.0f);
        lastUpdate = Now;

        MahonyQuaternionUpdate(accelX, accelY, accelZ, gyroX * DEG_TO_RAD, gyroY * DEG_TO_RAD, gyroZ * DEG_TO_RAD, magY, magX, magZ, deltat);
        auto res = getQ();

        q._f[0] = res[0];
        q._f[1] = res[1];
        q._f[2] = res[2];
        q._f[3] = res[3];
        accel.val[0] = imu.accData[0];
        accel.val[1] = imu.accData[1];
        accel.val[2] = imu.accData[2];
    }

    // Check for new data in the FIFO
    //if (imu.fifoAvailable())
    //{
    //    // Use dmpUpdateFifo to update the ax, gx, mx, etc. values
    //    if (imu.dmpUpdateFifo() == INV_SUCCESS)
    //    {
    //        // computeEulerAngles can be used -- after updating the
    //        // quaternion values -- to estimate roll, pitch, and yaw
    //        imu.computeEulerAngles();
    //        // After calling dmpUpdateFifo() the ax, gx, mx, etc. values
    //        // are all updated.
    //        // Quaternion values are, by default, stored in Q30 long
    //        // format. calcQuat turns them into a float between -1 and 1
    //        float q0 = imu.calcQuat(imu.qw);
    //        float q1 = imu.calcQuat(imu.qx);
    //        float q2 = imu.calcQuat(imu.qy);
    //        float q3 = imu.calcQuat(imu.qz);
    //    }
    //}
#endif
}

IMU::IMU()
{
#ifndef BAIT
    imu.begin();
    imu.setSensors(INV_XYZ_GYRO | INV_XYZ_ACCEL | INV_XYZ_COMPASS);

    imu.setGyroFSR(2000); // Set gyro to 2000 dps
                          // Accel options are +/- 2, 4, 8, or 16 g
    imu.setAccelFSR(2); // Set accel to +/-2g
    imu.setLPF(5); // Set LPF corner frequency to 5Hz
    imu.setSampleRate(10); // Set sample rate to 10Hz
    imu.setCompassSampleRate(10); // Set mag rate to 10Hz

    //imu.dmpBegin(DMP_FEATURE_6X_LP_QUAT | // Enable 6-axis quat
    //    DMP_FEATURE_GYRO_CAL, // Use gyro calibration
    //    10); // Set DMP FIFO rate to 10 Hz
    //         // DMP_FEATURE_LP_QUAT can also be used. It uses the 
    //         // accelerometer in low-power mode to estimate quat's.
    //         // DMP_FEATURE_LP_QUAT and 6X_LP_QUAT are mutually exclusive
#endif
}

#ifdef BAIT 
#include "MPU6050_6Axis_MotionApps20.h"

MPU6050 mpu;
volatile bool mpuInterrupt = false;
void dmpDataReady()
{
    mpuInterrupt = true;
}

#define LED_PIN 13 
bool blinkState = false;

uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

                        // orientation/motion vars
Quaternion qa;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
#endif 

Bait::Bait()
{
#ifdef BAIT 
    //Fastwire::setup(400, true);
    Wire.begin();
    TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)

    mpu.initialize();
    mpu.testConnection();
    mpu.dmpInitialize();

    mpu.setXGyroOffset(220);
    mpu.setYGyroOffset(76);
    mpu.setZGyroOffset(-85);
    mpu.setZAccelOffset(1788);

    mpu.setDMPEnabled(true);

    attachInterrupt(0, dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    packetSize = mpu.dmpGetFIFOPacketSize();
#endif 
}

void Bait::Tick()
{
#ifdef BAIT
    // wait for MPU interrupt or extra packet(s) available
    while (!mpuInterrupt && fifoCount < packetSize)
    {
        // other program behavior stuff here
        // if you are really paranoid you can frequently test in between other stuff to see if mpuInterrupt is true, and if so, "break;" from the while() loop to immediately process the MPU data
    }

    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    fifoCount = mpu.getFIFOCount();
    if ((mpuIntStatus & 0x10) || fifoCount == 1024)
    {
        mpu.resetFIFO();
    }
    else if (mpuIntStatus & 0x02)
    {
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        mpu.getFIFOBytes(fifoBuffer, packetSize);

        fifoCount -= packetSize;

        mpu.dmpGetQuaternion(&qa, fifoBuffer);
        q._f[0] = qa.w;
        q._f[1] = qa.x;
        q._f[2] = qa.y;
        q._f[3] = qa.z;
        mpu.dmpGetAccel(&aa, fifoBuffer);
        const float sens = 16384.f;
        humanAcc[0] = float(aa.x) / sens * 1000.f;
        humanAcc[1] = float(aa.y) / sens * 1000.f;
        humanAcc[2] = float(aa.z) / sens * 1000.f;
        mpu.dmpGetGravity(&gravity, &qa);
        mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
        mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &qa);
        accel.val[0] = int16_t(float(aaWorld.x) / sens * 100.f);
        accel.val[1] = int16_t(float(aaWorld.y) / sens * 100.f);
        accel.val[2] = int16_t(float(aaWorld.z) / sens * 100.f);

        mpu.dmpGetQuaternion(q16.q, fifoBuffer);

        mpu.dmpGetGravity(&gravity, &qa);
        mpu.dmpGetYawPitchRoll(ypr, &qa, &gravity);
        yaw = ypr[0] * 180 / M_PI;
        pitch = ypr[1] * 180 / M_PI;
        roll = ypr[2] * 180 / M_PI;

        blinkState = !blinkState;
        digitalWrite(LED_PIN, blinkState);
    }
#endif 
}