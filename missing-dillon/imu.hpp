#pragma once
#include "common.hpp"


class IMU
{
public:
    IMU();
    virtual void Tick();
    int  Read();
    u32 m_okReads = 0;

    Quat q;
    Quat16 q16;
    Accel accel;
    float humanAcc[3];
    float yaw, pitch, roll;
    bool ready = false;
};

class Bait : public IMU
{
public:
    Bait();
    void Tick();
};
