#include "cansat.hpp"
#include "bmp.hpp"
#include "imu.hpp"

//#define OUTPUT_SERIAL
//#define OUTPUT_HUMAN        !!!! WILL HAVE TO RESET BOARD IF USED !!!!
#include <Arduino.h> // Serial, millis()


Cansat::Cansat()
{
#ifdef OUTPUT_SERIAL
    Serial.begin(9600);
#endif

    imu = new Bait();
    bmp = new BmpSensor();

    auto info = RadioInitInfo()
        .SetNodeId(2)
        .SetNetworkId(100)
        .SetBitrate(Bitrate::RATE_4800);
    m_radio.Init(info);
}

void Cansat::Tick()
{
#ifdef DEBUG
    unsigned long start = micros();
    imu.Tick();
    if (!ready) return;
    Serial.print("IMU: "); unsigned long dtIMU = micros() - start; Serial.print(dtIMU);
    start = micros();
    bmp.Tick();
    Serial.print(" BMP: "); unsigned long dtBMP = micros() - start; Serial.print(dtBMP);
    start = micros();
    radio.Tick();
    Serial.print(" Output: "); unsigned long dtRadio = micros() - start; Serial.print(dtRadio);
    Serial.print(" Total: "); Serial.println(dtIMU + dtBMP + dtRadio);
#else
    imu->Tick();
    bmp->Tick();
    #ifdef OUTPUT_HUMAN
        char buffer[500];
        int ts = millis();
        int pres = int(bmp->pressure);
        int temp = int(bmp->temp);
        int sz = sprintf(buffer, "Timestamp: %d ms\nPressure: %d mbar Temperature: %d deg C\nYaw: %d deg Pitch: %d deg Roll: %d deg\nAcceleration: %d mg %d mg %d mg\n", 
                                          ts, pres, temp, int(imu->yaw), int(imu->pitch), int(imu->roll), int(imu->accel.val[0] * 10), int(imu->accel.val[1] * 10), int(imu->accel.val[2] * 10));
        #ifdef OUTPUT_SERIAL
            Serial.print(buffer);
        #else
            radio->Tick(buffer, sz);
        #endif
    #else 
        //Pack pack(float(bmp->pressure), float(bmp->temp), imu->yaw, imu->pitch, imu->roll, millis());
        Pack pack(float(bmp->pressure), float(bmp->temp), imu->q, imu->accel, millis());
        #ifdef OUTPUT_SERIAL
                Serial.write(pack.asBytes, Pack::packSz);
        #else
            m_radio.send(1, pack.asBytes, Pack::packSz);
        #endif
    #endif
#endif
}
