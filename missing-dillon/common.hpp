#pragma once

#include <stdint.h>
#include <string.h>


using u8 = uint8_t;
//using u16 = uint16_t; redefinition in some Arduino header
using u32 = uint32_t;
using u64 = uint64_t;

using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;

struct Quat
{
    union
    {
        // w x y z 
        float _f[4];
        long  _l[4];
    };
};

struct Quat16
{
    i16 q[4];
};

struct Accel
{
    i16 val[3];
};

//struct Attitude
//{
//    int16_t val[3]; // yaw, pitch, roll
//};

struct Pack
{
    // Head:       2 
    // Timestamp:  4
    // Quat: 4x4 = 16
    // Temp:       2
    // Pres:       2
    // Acce: 2x3 = 6
    // Trail:      2
    //static const u32 packSz = 16;
    //union
    //{ 
    //    u8 asBytes[packSz];
    //    struct 
    //    {
    //        u8 head[2];
    //        uint16_t timestamp; // dts, add up to the real timestamp at the destination, avoid 4 bytes to avoid padding the head 
    //        //Quat q;
    //        //Quat16 q;
    //        //Attitude att;
    //        //i16 att[3];
    //        i16 yaw, pitch, roll;
    //        uint16_t temp;
    //        uint16_t pres;
    //        //Accel acc;
    //        u8 trail[2];
    //    } val;
    //};

    static const u32 packSz = 34;
    union
    {
        u8 asBytes[packSz];
        struct
        {
            u8 head[2]; // pad 2
            u32 timestamp;
            Quat q;
            uint16_t temp;
            uint16_t pres;
            Accel accel;
            u8 trail[2];
        } val;
    };

    Pack::Pack()
    {
        memset(this, 0, sizeof(*this));
    }

    // Don't send the whole dataset each tick 
    // Frequent: Quats: the 32-bit values in the IMU buffer (16 bytes)
    //           Accel: the 16-bit values in the IMU buffer (6 bytes)
    //           Timestamp: 32-bit value (4 bytes)
    //           => 26 bytes 
    // Infreq:   Temp + Pressure (4 bytes)
    //           => 30 bytes 
    // Flags:    4 bytes 
    //           => 34 bytes 

    //Pack::Pack(float pressure, float temp, Quat16 q, u32 timestamp)
    //{
    //    val.head[0] = '$'; val.head[1] = 0x02;
    //    val.trail[0] = 0x99; val.trail[1] = 0xA7;
    //    val.timestamp = (uint16_t)timestamp;
    //    val.q = q;
    //    val.temp = uint16_t(temp * 100);
    //    val.pres = uint16_t(pressure * 10);
    //    //val.acc = acc;
    //}

    Pack::Pack(float pressure, float temp, Quat q, Accel acc, u32 timestamp)
    {
        val.head[0] = '$'; val.head[1] = 0x02;
        val.trail[0] = 0x99; val.trail[1] = 0xA7;
        val.timestamp = timestamp;
        val.q = q;
        val.temp = uint16_t(temp * 100);
        val.pres = uint16_t(pressure * 10);
        val.accel = acc;
    }

    /*Pack::Pack(float pressure, float temp, float yaw, float pitch, float roll, u32 timestamp)
    {
        val.head[0] = '$'; val.head[1] = 0x02;
        val.trail[0] = 0x99; val.trail[1] = 0xA7;
        val.timestamp = uint16_t(timestamp);
        val.yaw = i16(yaw * 10.f);
        val.pitch = i16(pitch * 10.f);
        val.roll = i16(roll * 10.f);
        val.temp = uint16_t(temp * 100.f);
        val.pres = uint16_t(pressure * 10.f);
    }*/
};