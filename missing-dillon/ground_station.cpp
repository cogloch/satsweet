#include "ground_station.hpp"
#include <Arduino.h>   // UART 


GroundStation::GroundStation()
{
    Serial.begin(9600);

    auto info = RadioInitInfo()
        .SetNodeId(1)
        .SetNetworkId(100)
        .SetBitrate(Bitrate::RATE_4800);
    m_radio.Init(info);
}

void GroundStation::Tick()
{
    if (!m_radio.receiveDone()) return;

    for (u8 i = 0; i < m_radio.DATALEN; ++i)
        Serial.write((u8)m_radio.DATA[i]);
}