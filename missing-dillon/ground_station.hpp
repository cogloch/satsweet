#pragma once
#include "radio.hpp"


class GroundStation
{
public:
    GroundStation();
    void Tick();

private:
    Radio m_radio;
};