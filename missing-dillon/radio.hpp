//
// Largely a clone of lowpowerlab's RFM69 library: http://lowpowerlab.com/, with a changed API 
//
#pragma once
#include "common.hpp"
#include "radio_registers.hpp"


#define RF69_MAX_DATA_LEN       61 // to take advantage of the built in AES/CRC we want to limit the frame size to the internal FIFO size (66 bytes - 3 bytes overhead - 2 bytes crc)
#define RF69_SPI_CS             10 // SS is the SPI slave select pin, for instance D10 on ATmega328

// INT0 on AVRs should be connected to RFM69's DIO0 (ex on ATmega328 it's D2, on ATmega644/1284 it's D2)
#if defined(__AVR_ATmega168__) || defined(__AVR_ATmega328P__) || defined(__AVR_ATmega88) || defined(__AVR_ATmega8__) || defined(__AVR_ATmega88__)
  #define RF69_IRQ_PIN          2
  #define RF69_IRQ_NUM          0
#elif defined(__AVR_ATmega644P__) || defined(__AVR_ATmega1284P__)
  #define RF69_IRQ_PIN          2
  #define RF69_IRQ_NUM          2
#elif defined(__AVR_ATmega32U4__)
  #define RF69_IRQ_PIN          7
  #define RF69_IRQ_NUM          4
#else 
  #define RF69_IRQ_PIN          2
  #define RF69_IRQ_NUM          0  
#endif


#define CSMA_LIMIT              -90 // upper RX signal sensitivity threshold in dBm for carrier sense access
#define RF69_MODE_SLEEP         0 // XTAL OFF
#define RF69_MODE_STANDBY       1 // XTAL ON
#define RF69_MODE_SYNTH         2 // PLL ON
#define RF69_MODE_RX            3 // RX MODE
#define RF69_MODE_TX            4 // TX MODE

// available frequency bands
#define RF69_315MHZ            31 // non trivial values to avoid misconfiguration
#define RF69_433MHZ            43
#define RF69_868MHZ            86
#define RF69_915MHZ            91

#define null                  0
#define COURSE_TEMP_COEF    -90 // puts the temperature reading in the ballpark, user can fine tune the returned value
#define RF69_BROADCAST_ADDR 255
#define RF69_CSMA_LIMIT_MS 1000
#define RF69_TX_LIMIT_MS   1000
#define RF69_FSTEP  61.03515625 // == FXOSC / 2^19 = 32MHz / 2^19 (p13 in datasheet)

//qbcan specific flags
#define FREQUENCY   RF69_433MHZ
#define IS_RFM69HW

enum class Bitrate : uint16_t
{
    RATE_1200   = (RF_BITRATEMSB_1200 << 8)   | RF_BITRATELSB_1200,
    RATE_2400   = (RF_BITRATEMSB_2400 << 8)   | RF_BITRATELSB_2400,
    RATE_4800   = (RF_BITRATEMSB_4800 << 8)   | RF_BITRATELSB_4800,
    RATE_9600   = (RF_BITRATEMSB_9600 << 8)   | RF_BITRATELSB_9600,
    RATE_19200  = (RF_BITRATEMSB_19200 << 8)  | RF_BITRATELSB_19200,
    RATE_38400  = (RF_BITRATEMSB_38400 << 8)  | RF_BITRATELSB_38400,
    
    RATE_38323  = (RF_BITRATEMSB_38323 << 8)  | RF_BITRATELSB_38323,
    
    RATE_34482  = (RF_BITRATEMSB_34482 << 8)  | RF_BITRATELSB_34482,
    
    RATE_76800  = (RF_BITRATEMSB_76800 << 8)  | RF_BITRATELSB_76800,
    RATE_153600 = (RF_BITRATEMSB_153600 << 8) | RF_BITRATELSB_153600,
    RATE_57600  = (RF_BITRATEMSB_57600 << 8)  | RF_BITRATELSB_57600,
    RATE_115200 = (RF_BITRATEMSB_115200 << 8) | RF_BITRATELSB_115200,
    RATE_12500  = (RF_BITRATEMSB_12500 << 8)  | RF_BITRATELSB_12500,
    RATE_25000  = (RF_BITRATEMSB_25000 << 8)  | RF_BITRATELSB_25000,
    RATE_50000  = (RF_BITRATEMSB_50000 << 8)  | RF_BITRATELSB_50000,
    RATE_100000 = (RF_BITRATEMSB_100000 << 8) | RF_BITRATELSB_100000,
    RATE_150000 = (RF_BITRATEMSB_150000 << 8) | RF_BITRATELSB_150000,
    RATE_200000 = (RF_BITRATEMSB_200000 << 8) | RF_BITRATELSB_200000,
    RATE_250000 = (RF_BITRATEMSB_250000 << 8) | RF_BITRATELSB_250000,
    RATE_300000 = (RF_BITRATEMSB_300000 << 8) | RF_BITRATELSB_300000
};

struct RadioInitInfo
{
    u8 nodeId       = 1;
    u8 networkId    = 100;
    Bitrate bitrate = Bitrate::RATE_1200;

    RadioInitInfo& SetNodeId(u8 nodeId_)
    {
        nodeId = nodeId_;
        return *this;
    }

    RadioInitInfo& SetNetworkId(u8 networkId_)
    {
        networkId = networkId_;
        return *this;
    }

    RadioInitInfo& SetBitrate(Bitrate bitrate_)
    {
        bitrate = bitrate_;
        return *this;
    }
};

class Radio
{
public:
    static volatile u8 DATA[RF69_MAX_DATA_LEN]; // recv/xmit buf, including header & crc bytes
    static volatile u8 DATALEN;
    static volatile u8 SENDERID;
    static volatile u8 TARGETID; // should match _address
    static volatile u8 PAYLOADLEN;
    static volatile u8 ACK_REQUESTED;
    static volatile u8 ACK_RECEIVED; // should be polled immediately after sending a packet with ACK request
    static volatile i16 RSSI; // most accurate RSSI during reception (closest to the reception)
    static volatile u8 _mode; // should be protected?

    Radio();
    bool Init(const RadioInitInfo&);

    bool canSend();
    void send(u8 toAddress, const void* buffer, u8 bufferSize, bool requestACK=false);
    bool sendWithRetry(u8 toAddress, const void* buffer, u8 bufferSize, u8 retries=2, u8 retryWaitTime=40); // 40ms roundtrip req for 61byte packets
    bool receiveDone();
    bool ACKReceived(u8 fromNodeID);
    bool ACKRequested();
    void sendACK(const void* buffer = "", u8 bufferSize=0);
    void encrypt(const char* key);
    void setCS(u8 newSPISlaveSelect);
    i16 readRSSI(bool forceTrigger=false);
    void promiscuous(bool onOff=true);
    void setHighPower(bool onOFF=true); // has to be called after initialize() for RFM69HW
    void setPowerLevel(u8 level); // reduce/increase transmit power level
    void sleep();
    u8 readTemperature(u8 calFactor=0); // get CMOS temperature (8bit)
    void rcCalibration(); // calibrate the internal RC oscillator for use in wide temperature variations - see datasheet section [4.3.5. RC Timer Accuracy]

    // allow hacking registers by making these public
    u8 readReg(u8 addr);
    void writeReg(u8 addr, u8 val);
    void readAllRegs();

private:
    static void isr0();
    void virtual interruptHandler();
    void sendFrame(u8 toAddress, const void* buffer, u8 size, bool requestACK=false, bool sendACK=false);

    static Radio* selfPointer;
    u8 _slaveSelectPin;
    u8 _interruptPin;
    u8 _interruptNum;
    u8 _address;
    bool _promiscuousMode;
    u8 _powerLevel;
    bool _isRFM69HW;
    u8 _SPCR;
    u8 _SPSR;

    void receiveBegin();
    void setMode(u8 mode);
    void setHighPowerRegs(bool onOff);
    void select();
    void unselect();
};

