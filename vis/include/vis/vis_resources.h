#pragma once
#include "vis.h"
#include <string>
#include <memory>
#include <optional>
#include "model.h"


namespace nvg
{
	struct Context;
}

namespace vis
{
	// TODO temp
	extern nvg::Context* g_nvcontext;

	namespace fonts
	{
		int VIS_API GetFontByName(const std::string& name);
		// TOOD temp
		void VIS_API * GetFont(int id);
		int VIS_API LoadFromFile(const std::string& path, const std::string& name = "");
		int VIS_API LoadFromMem(const std::string& name, u8* data, size_t dataSz, size_t freeData);
	}

	namespace meshes
	{
		std::shared_ptr<Model> VIS_API LoadMesh(const std::string& path);
		std::optional<std::shared_ptr<Model>> VIS_API GetMesh(const std::string& path);
	}
}
