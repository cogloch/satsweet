#pragma once
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include "vis.h"


struct VIS_API Vertex
{
    // position
    glm::vec3 Position;
    // normal
    glm::vec3 Normal;
    // texCoords
    glm::vec2 TexCoords;
    // tangent
    glm::vec3 Tangent;
    // bitangent
    glm::vec3 Bitangent;
};

struct VIS_API Texture
{
    unsigned int id;
    std::string type;
    std::string path;
};

class VIS_API Mesh 
{
public:
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;
    unsigned int VAO;

	Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);
	void Draw(unsigned int shader);

private:
    unsigned int VBO, EBO;

	void SetupMesh();
};