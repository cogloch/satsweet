#pragma once

#ifdef vis_EXPORTS  
#define VIS_API __declspec(dllexport)   
#else  
#define VIS_API __declspec(dllimport)   
#endif  


#include <cstdint>

using u8 = uint8_t;


struct Extent
{
	Extent(const int width, const int height) : width(width), height(height) {}
	int width, height;
};
