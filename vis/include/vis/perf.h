#pragma once
#include <string>


namespace nvg { struct Context; }

const size_t GRAPH_HISTORY_COUNT = 100;
struct PerfGraph 
{
	enum class RenderStyle
	{
		FPS,
		MS,
		PERCENT
	} style;
	
	std::string name;
	float values[GRAPH_HISTORY_COUNT];
	int head;

	PerfGraph(const RenderStyle style, const std::string& name);
	void Update(const float frameTime);
	float GetAverage();
	void Render(nvg::Context*, const float x, const float y);
};

const size_t GPU_QUERY_COUNT = 5;
struct GPUTimer 
{
	int supported;
	int cur, ret;
	unsigned int queries[GPU_QUERY_COUNT];

	GPUTimer();
	void Start();
	int Stop(float* times, int maxTimes);
};

// Frame time, CPU time, GPU time 
struct PerfPack
{
	PerfPack();
	void Reset();   // Start of frame 
	void Elapsed(); // End of frame 
	void Render(nvg::Context*);

	GPUTimer gpuTimer;
	double cpuTime; // CPU time taken excluding swap buffers (as the swap may wait for GPU)
	double prevTime, curTime, dt;
	PerfGraph fps, cpuGraph, gpuGraph;
};
