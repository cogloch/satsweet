#pragma once
#include "gl/gl_core.hpp"
#include "glm/glm.hpp"


struct Shader
{
    GLuint program;

    // Name is shared between stages, only the extensions differ (e.g. phong.vert, phong.frag)
    explicit Shader(const std::string& filename);
    GLint GetUniformLoc(const std::string& name) const;

    void Use() const;

    static void SetMat4(const GLint loc, const glm::mat4&);
    void SetMat4(const std::string& locName, const glm::mat4&) const;

    static void SetInt(const GLint loc, const int);
    void SetInt(const std::string &locName, const int) const;

    static void SetFloat(const GLint loc, const float);
    void SetFloat(const std::string& locName, const float) const;

    static void SetVec2(const GLint loc, const glm::vec2&);
    void SetVec2(const std::string& locName, const glm::vec2&) const;

    static void SetVec3(const GLint loc, const glm::vec3&);
    void SetVec3(const std::string& locName, const glm::vec3&) const;
    
private:
    static std::string ReadFile(const std::string& filename);
    static std::optional<GLuint> CompileStage(const GLenum stage, const char* source);
    static std::optional<GLuint> CreateStage(const GLenum stage, const std::string& filename);
};