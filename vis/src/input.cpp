#include "stdafx.h"
#include "input.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>


InputState::InputState()
	: window(nullptr)
	, mouseX(0.0), mouseY(0.0)
{
}

void InputState::Update()
{
	glfwGetCursorPos(window, &mouseX, &mouseY);
}