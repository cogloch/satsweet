#include "stdafx.h"
#include "capture.h"

#include <gl/gl_core.hpp>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <nanovg/stb_image_write.h>


int mini(const int a, const int b) { return a < b ? a : b; }

ImageBuffer::ImageBuffer(const int width, const int height)
	: data(nullptr)
	, width(width), height(height)
{
	data = new u8[width * height * 4];
	if (!data)
		throw "ImageBuffer: alloc failed";
}

ImageBuffer::~ImageBuffer()
{
	if (data)
		delete[] data;
}

void ImageBuffer::FlipHorizontal()
{
    const auto stride = width * 4;

	auto i = 0, j = height - 1;
	while (i < j)
	{
        const auto ri = &data[i * stride];
		const auto rj = &data[j * stride];
		for (int k = 0; k < stride; ++k)
		{
			const auto temp = ri[k];
			ri[k] = rj[k];
			rj[k] = temp;
		}

		i++;
		j--;
	}
}

void ImageBuffer::UnpremultiplyAlpha()
{
	const auto stride = width * 4;

	// Unpremultiply
	for (auto y = 0; y < height; ++y)
	{
		auto row = &data[y * stride];
		for (auto x = 0; x < width; ++x)
		{
			const auto r = row[0], g = row[1], b = row[2], a = row[3];
			if (a != 0)
			{
				row[0] = mini(r * 255 / a, 255);
				row[1] = mini(g * 255 / a, 255);
				row[2] = mini(b * 255 / a, 255);
			}
			row += 4;
		}
	}

	// Defringe
	for (auto y = 0; y < height; ++y)
	{
		auto row = &data[y * stride];
		for (auto x = 0; x < width; ++x)
		{
			int r = 0, g = 0, b = 0, a = row[3], n = 0;
			if (a == 0)
			{
				if (x - 1 > 0 && row[-1] != 0)
				{
					r += row[-4];
					g += row[-3];
					b += row[-2];
					n++;
				}
				if (x + 1 < width && row[7] != 0)
				{
					r += row[4];
					g += row[5];
					b += row[6];
					n++;
				}
				if (y - 1 > 0 && row[-stride + 3] != 0)
				{
					r += row[-stride];
					g += row[-stride + 1];
					b += row[-stride + 2];
					n++;
				}
				if (y + 1 < height && row[stride + 3] != 0)
				{
					r += row[stride];
					g += row[stride + 1];
					b += row[stride + 2];
					n++;
				}
				if (n > 0)
				{
					row[0] = r / n;
					row[1] = g / n;
					row[2] = b / n;
				}
			}
			row += 4;
		}
	}
}

void ImageBuffer::SetAlpha(const u8 alpha)
{
	const auto stride = width * 4;

	for (auto y = 0; y < height; ++y)
	{
		const auto row = &data[y * stride];
		for (auto x = 0; x < width; ++x)
		{
			row[x * 4 + 3] = alpha;
		}
	}
}

void SaveScreenShot(const int width, const int height, const std::string& name)
{
	ImageBuffer image(width, height);
	gl::ReadPixels(0, 0, width, height, gl::RGBA, gl::UNSIGNED_BYTE, image.data);
    image.SetAlpha(255);
	image.FlipHorizontal();
	stbi_write_png(name.c_str(), width, height, 4, image.data, width * 4);
}

