#include "stdafx.h"
#include "mesh.h"

#include <gl/gl_core.hpp>


Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures)
{
	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;

	SetupMesh();
}

void Mesh::Draw(unsigned int shader)
{
	/*unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;
	unsigned int normalNr = 1;
	unsigned int heightNr = 1;
	for (unsigned int i = 0; i < textures.size(); i++)
	{
	gl::ActiveTexture(gl::TEXTURE0 + i);

	string number;
	string name = textures[i].type;
	if (name == "texture_diffuse")
	number = std::to_string(diffuseNr++);
	else if (name == "texture_specular")
	number = std::to_string(specularNr++);
	else if (name == "texture_normal")
	number = std::to_string(normalNr++);
	else if (name == "texture_height")
	number = std::to_string(heightNr++);

	gl::Uniform1i(gl::GetUniformLocation(shader, (name + number).c_str()), i);
	gl::BindTexture(gl::TEXTURE_2D, textures[i].id);
	}*/

	gl::BindVertexArray(VAO);
	gl::DrawElements(gl::TRIANGLES, indices.size(), gl::UNSIGNED_INT, 0);
	gl::BindVertexArray(0);

	//gl::ActiveTexture(gl::TEXTURE0);
}

void Mesh::SetupMesh()
{
	gl::GenVertexArrays(1, &VAO);
	gl::GenBuffers(1, &VBO);
	gl::GenBuffers(1, &EBO);

	gl::BindVertexArray(VAO);
	gl::BindBuffer(gl::ARRAY_BUFFER, VBO);
	gl::BufferData(gl::ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], gl::STATIC_DRAW);

	gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, EBO);
	gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], gl::STATIC_DRAW);

	// vertex Positions
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE_, sizeof(Vertex), (void*)0);
	// vertex normals
	gl::EnableVertexAttribArray(1);
	gl::VertexAttribPointer(1, 3, gl::FLOAT, gl::FALSE_, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
	// vertex texture coords
	gl::EnableVertexAttribArray(2);
	gl::VertexAttribPointer(2, 2, gl::FLOAT, gl::FALSE_, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));
	// vertex tangent
	gl::EnableVertexAttribArray(3);
	gl::VertexAttribPointer(3, 3, gl::FLOAT, gl::FALSE_, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));
	// vertex bitangent
	gl::EnableVertexAttribArray(4);
	gl::VertexAttribPointer(4, 3, gl::FLOAT, gl::FALSE_, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));

	gl::BindVertexArray(0);
}