#pragma once
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

#include <optional>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cassert>
#include <string>
#include <vector>
#include <cstdio>
#include <thread>
#include <mutex>
#include <ctime>
#include <map>


using u8 = uint8_t;