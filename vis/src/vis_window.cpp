#include "stdafx.h"
#include "vis_window.h"
#include <gl/gl_core.hpp>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include "perf.h"
#include "input.h"
#include "ui.h"
#include "model.h"
#include "serial_com.h"
#include "cansat.h"
#include "timing.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>

#include <nanovg/nanovg.h>
#include <nanovg/nanovg_gl.h>

#include "shader.h"
#include "camera.h"

#include "vis_resources.h"


double Sealevel(const double P, const double A)
{
    return(P / pow(1 - (A / 44330.0), 5.255));
}

double EstimateAltitude(const double P, const double P0)
{
    return(44330.0*(1 - pow(P / P0, 1 / 5.255)));
}

struct FbState
{
    int winWidth, winHeight;
    int fbWidth, fbHeight;
    float pxRatio; // Pixel ratio for hi-dpi devices 
    GLFWwindow* window;

    void Update()
    {
        glfwGetWindowSize(window, &winWidth, &winHeight);
        glfwGetFramebufferSize(window, &fbWidth, &fbHeight);
        pxRatio = float(fbWidth) / float(winWidth);

        gl::Viewport(0, 0, fbWidth, fbHeight);
    }
};

bool VisWindow::s_glfwInit = false;

nvg::Context* vg = nullptr;

TimelineMgr timelines;
Cansat sat;

PerfPack perf;
InputState inputState;
FbState fbState;
TimerMgr timerMgr;

SerialCom* serial;

CanState realState, toRender;
std::vector<CanState> states;

TimerHandle timelineTimer;

int connectedPort = 0;

Camera camera(glm::vec3(0.0f, 0.0f, 10.0f));
float lastX = 1024.f / 2.0;
float lastY = 768.f / 2.0;
bool firstMouse = true;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

bool uiInit = false;
Screen screen;

std::shared_ptr<Button> serialConnectBtn;
glm::vec2 connectBtnPos = { 48, 500 };
glm::vec2 connectBtnExtent = { 140, 28 };

std::shared_ptr<Button> prevPortBtn;
glm::vec2 prevPortBtnPos = { 10, 500 };
glm::vec2 prevPortBtnExtent = { 28, 28 };

std::shared_ptr<Button> nextPortBtn;
glm::vec2 nextPortBtnPos = { 198, 500 };
glm::vec2 nextPortBtnExtent = { 28, 28 };

std::shared_ptr<Button> loadBtn;
glm::vec2 loadBtnPos = { 10, 654 };
glm::vec2 loadBtnExtent = { 80, 20 };

std::shared_ptr<Button> recordBtn;
glm::vec2 recordBtnPos = { 10, 677 };
glm::vec2 recordBtnExtent = { 80, 20 };

std::shared_ptr<Button> exportBtn;
glm::vec2 exportBtnPos = { 10, 700 };
glm::vec2 exportBtnExtent = { 80, 20 };

std::shared_ptr<Button> playBtn;
glm::vec2 playBtnPos = { 10, 723 };
glm::vec2 playBtnExtent = { 80, 20 };

std::shared_ptr<Slider> slider;
glm::vec2 sliderPos = { 120, 680 };
glm::vec2 sliderExtent = { 800, 40 };

int curInputPort = 11;

void ConnectSerial(const int port)
{
    try
    {
        serial = new SerialCom(port, 115200);
    }
    catch (...)
    {
        std::cout << "Failed connecting to port " << port << '\n';
        return;
    }

    connectedPort = port;
    std::thread serialListenThread(&SerialCom::Listen, serial);
    serialListenThread.detach();
}

void KillSerial()
{
}

void InitUI()
{
    uiInit = true;

    serialConnectBtn = std::make_shared<Button>();
    serialConnectBtn->text = "Connect";
    serialConnectBtn->pos = connectBtnPos;
    serialConnectBtn->extent = connectBtnExtent;
    serialConnectBtn->callback = [](void*)
    {
        ConnectSerial(curInputPort);
        if (serialConnectBtn->state != Button::State::DISABLED && connectedPort)
        {
            serialConnectBtn->ToggleEnable();
            prevPortBtn->ToggleEnable();
            nextPortBtn->ToggleEnable();
        }
    };
    screen.elements.push_back(serialConnectBtn);

    prevPortBtn = std::make_shared<Button>();
    prevPortBtn->text = "<";
    prevPortBtn->pos = prevPortBtnPos;
    prevPortBtn->extent = prevPortBtnExtent;
    prevPortBtn->callback = [](void*) { curInputPort = curInputPort > 1 ? curInputPort - 1 : 1; };
    screen.elements.push_back(prevPortBtn);

    nextPortBtn = std::make_shared<Button>();
    nextPortBtn->text = ">";
    nextPortBtn->pos = nextPortBtnPos;
    nextPortBtn->extent = nextPortBtnExtent;
    nextPortBtn->callback = [](void*) { curInputPort++; };
    screen.elements.push_back(nextPortBtn);

    loadBtn = std::make_shared<Button>();
    loadBtn->text = "Load";
    loadBtn->pos = loadBtnPos;
    loadBtn->extent = loadBtnExtent;
    loadBtn->callback = [](void*) { timelines.OnLoad(); };
    screen.elements.push_back(loadBtn);

    recordBtn = std::make_shared<Button>();
    recordBtn->text = "Record";
    recordBtn->pos = recordBtnPos;
    recordBtn->extent = recordBtnExtent;
    recordBtn->callback = [](void*) { timelines.OnRecordKey(); };
    screen.elements.push_back(recordBtn);

    exportBtn = std::make_shared<Button>();
    exportBtn->text = "Export";
    exportBtn->pos = exportBtnPos;
    exportBtn->extent = exportBtnExtent;
    exportBtn->callback = [](void*) { timelines.OnExport(); };
    screen.elements.push_back(exportBtn);

    playBtn = std::make_shared<Button>();
    playBtn->text = "Play";
    playBtn->pos = playBtnPos;
    playBtn->extent = playBtnExtent;
    playBtn->callback = [](void*) { timelines.OnPlayKey(); };
    screen.elements.push_back(playBtn);

    slider = std::make_shared<Slider>();
    slider->pos = sliderPos;
    slider->extent = sliderExtent;
    slider->callback = [](void*) { timelines.JumpTo(slider->knobPos); };
    screen.elements.push_back(slider);
}

void KeyCb(GLFWwindow* window, const int key, int scancode, const int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
    
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

void MouseCb(GLFWwindow* window, const double xpos, const double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    const auto xoffset = xpos - lastX;
    const auto yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS)
        camera.ProcessMouseMovement(xoffset, yoffset);

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS && slider->dragging)
    {
        slider->knobPos = (xpos - slider->pos.x) / slider->extent.x;
        slider->knobPos = std::clamp(slider->knobPos, 0.f, 1.f);
        slider->OnDrag(xpos);
    }
}

void ClickCb(GLFWwindow* window, const int button, const int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        const glm::vec2 cursor(xpos, ypos);
    
        // this is embarassing
        if (action == GLFW_RELEASE)
        {
            if (serialConnectBtn->PointInBounds(cursor))
            {
                serialConnectBtn->OnRelease();

                if (serialConnectBtn->state != Button::State::DISABLED && connectedPort)
                {
                    serialConnectBtn->ToggleEnable();
                    prevPortBtn->ToggleEnable();
                    nextPortBtn->ToggleEnable();
                }
            }

            if (prevPortBtn->PointInBounds(cursor))
                prevPortBtn->OnRelease();

            if (nextPortBtn->PointInBounds(cursor))
                nextPortBtn->OnRelease();

            if (recordBtn->PointInBounds(cursor))
                recordBtn->OnRelease();

            if (exportBtn->PointInBounds(cursor))
                exportBtn->OnRelease();

            if (playBtn->PointInBounds(cursor))
                playBtn->OnRelease();

            if (loadBtn->PointInBounds(cursor))
                loadBtn->OnRelease();
        }
        else if (action == GLFW_PRESS)
        {
            if (serialConnectBtn->PointInBounds(cursor))
                serialConnectBtn->OnPress();

            if (prevPortBtn->PointInBounds(cursor))
                prevPortBtn->OnPress();

            if (nextPortBtn->PointInBounds(cursor))
                nextPortBtn->OnPress();

            if (recordBtn->PointInBounds(cursor))
                recordBtn->OnPress();
            
            if (exportBtn->PointInBounds(cursor))
                exportBtn->OnPress();

            if (playBtn->PointInBounds(cursor))
                playBtn->OnPress();

            if (loadBtn->PointInBounds(cursor))
                loadBtn->OnPress();

            slider->TestDrag(cursor);
        }
    }
}

void ScrollCb(GLFWwindow* window, double xoffset, const double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}

void ErrorCb(const int error, char const* desc)
{
	printf("GLFW error %d: %s\n", error, desc);
}

VisWindow::VisWindow()
	: m_window(nullptr)
{
	if (!s_glfwInit)
	{
		if (!glfwInit())
			throw "VisWindow: glfw failed to init";

		s_glfwInit = true;
		glfwSetErrorCallback(ErrorCb);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); 
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	m_window = glfwCreateWindow(1024, 768, "Missing Dillon 1.0", nullptr, nullptr);
	if (!m_window)
		throw "VisWindow: window creation failed";

	GLFWwindow* prevContext = glfwGetCurrentContext();
	glfwMakeContextCurrent(m_window);

	// Set callbacks
	glfwSetKeyCallback(m_window, KeyCb);
    glfwSetCursorPosCallback(m_window, MouseCb);
    glfwSetScrollCallback(m_window, ScrollCb);
    glfwSetMouseButtonCallback(m_window, ClickCb);

	// Set gl 
	int width, height;
	glfwGetFramebufferSize(m_window, &width, &height);
	gl::ClearColor(102.f/255.f, 84.f/255.f, 94.f/255.f, 1.f);
	gl::Viewport(0, 0, width, height);
	glfwSwapInterval(1);

#ifdef DEMO_MSAA
    vg = new nvg::Context(nvg::STENCIL_STROKES);
#else
    vg = new nvg::Context(nvg::ANTIALIAS | nvg::STENCIL_STROKES);
#endif

	vis::g_nvcontext = vg;

	glfwMakeContextCurrent(prevContext);

    MakeCurrent();

    inputState.window = m_window;
    fbState.window = m_window;

    sat.Init();

    timelineTimer = timerMgr.AddTimer();
    timerMgr.Pause(timelineTimer);
}

VisWindow::~VisWindow()
{
    delete vg;
	if(m_window)
		glfwDestroyWindow(m_window);
}

void VisWindow::MakeCurrent() const
{
	glfwMakeContextCurrent(m_window);
}

std::vector<u8> serialBuffer;

std::vector<CanState> stateQueue;
CanState lastCan;

std::vector<CanState> GetCanState()
{
    auto result = stateQueue; // Adds a duplicate on each tick 
    if (result.empty()) return result;
    if (result.front().dirty)
        result.erase(result.begin());
    if (stateQueue.size() > 1)
    {
        stateQueue.erase(stateQueue.begin(), stateQueue.end() - 1);
        stateQueue.front().dirty = true;
    }
    return result;
}

Graph presGraph("Pressure", 1500.f, Color(0, 0, 255, 128), "mb", 1), tempGraph("Temperature", 50.f, Color(255, 0, 0, 128), "deg C", 2);
Graph pitchGraph("Pitch", 360.f, Color(246, 224, 181, 128), "deg", 1, 50, false), rollGraph("Roll", 360.f, Color(246, 224, 181, 128), "deg", 1, 50, false), yawGraph("Yaw", 360.f, Color(246, 224, 181, 128), "deg", 1, 50, false);
Graph accXGraph("Accel X", 2000.f, Color(238, 169, 144, 128), "mg", 0, 50, false);
Graph accYGraph("Accel Y", 2000.f, Color(238, 169, 144, 128), "mg", 0, 50, false);
Graph accZGraph("Accel Z", 2000.f, Color(238, 169, 144, 128), "mg", 0, 50, false);

struct GraphTemp
{
	// wtf 
	// TODO: just kill myself already 
	Graph presGraph = Graph("Pressure", 1500.f, Color(0, 0, 255, 128), "mb", 1);
	Graph tempGraph = Graph("Temperature", 50.f, Color(255, 0, 0, 128), "deg C", 2);
	Graph pitchGraph = Graph("Pitch", 360.f, Color(246, 224, 181, 128), "deg", 1, 50, false);
	Graph rollGraph = Graph("Roll", 360.f, Color(246, 224, 181, 128), "deg", 1, 50, false);
	Graph yawGraph = Graph("Yaw", 360.f, Color(246, 224, 181, 128), "deg", 1, 50, false);
	Graph accXGraph = Graph("Accel X", 2000.f, Color(238, 169, 144, 128), "mg", 0, 50, false);
	Graph accYGraph = Graph("Accel Y", 2000.f, Color(238, 169, 144, 128), "mg", 0, 50, false);
	Graph accZGraph = Graph("Accel Z", 2000.f, Color(238, 169, 144, 128), "mg", 0, 50, false);
} graphTemp;

inline float Wrap180(const float x)
{
    if (x < -180.f)
        return  x + 360.f;
    if (x > 180.f)
        return x - 180.f;
    return x;
}

inline void SlerpLast(std::vector<CanState>& states, const int inbetweens)
{
    const auto increment = 1.f / (inbetweens + 1);
    const auto first = states[states.size() - 2];
    const auto second = states[states.size() - 1];
    const auto dt = second.timestamp - first.timestamp;
    const auto dtemp = second.temp - first.temp;
    const auto dpres = second.pressure - first.pressure;
    for (auto i = 1; i <= inbetweens; ++i)
    {
        auto newFrame = second;
        newFrame.quat = slerp(first.quat, second.quat, increment * i);
        newFrame.timestamp = first.timestamp + dt * increment * i;
        newFrame.temp = first.temp + dtemp * increment * i;
        newFrame.pressure = first.pressure + dpres * increment * i;
        states.insert(states.end() - 1, newFrame);
    }
}

struct CanFrame // CanState
{

};

struct DataContext
{
	enum class Source
	{
		NONE,
		LIVE,
		SAVED
	} m_source;

	DataContext()
		: m_source(Source::NONE)
	{
	}

	std::vector<CanState> m_states;
	size_t m_currentFrame;

	// From serial
	void Update()
	{
		// Copy the serial comm thread's buffer 
		std::vector<u8> newData;
		if (connectedPort)
			newData = serial->GetData();
		move(newData.begin(), newData.end(), back_inserter(serialBuffer));

		// Iterate through the existing buffer + new data copied from serial thread 
		size_t i = 0, toRemove = 0;
		const auto sz = serialBuffer.size();
		for (;;)
		{
			// Not enough bytes left for a complete packet
			if ((i + 34) >= sz) break;

			// Look for the start of a packet: $, 0x02
			while (serialBuffer[i++] != '$');
			if (serialBuffer[i++] != 0x02)
				continue;

			// The next 30 bytes should be the useful data 
			union Pack
			{
				struct
				{
					uint32_t ts;
					float q[4];
					uint16_t temp;
					uint16_t pressure;
					int16_t acc[3];
				};
				u8 bytes[30];
			} pack;
			for (int j = 0; j < 30; i++, j++)
				pack.bytes[j] = serialBuffer[i];

			glm::quat q;
			memcpy(&q, pack.q, 16);

			// If the next 2 bytes are not 0x99, 0xA7, ignore the recent "useful data"
			if (!(serialBuffer[i++] == 0x99 && serialBuffer[i++] == 0xA7))
				continue;

			// Complete pack read, remove from the buffer at least until the end of this pack
			toRemove = i - 1; // The pointer is currently set after the end of the pack 
			
			// Final useful state
			CanState state;
			state.timestamp = pack.ts;
			state.quat = q;
			state.temp = float(pack.temp) / 100.f;
			state.pressure = float(pack.pressure) / 10.f;
			state.acc = { float(pack.acc[0]) / 100.f, float(pack.acc[1]) / 100.f, float(pack.acc[2] /*+ 2*/) / 100.f }; // Where did the + 2 come from? Indexing?

			// Skip if the new state seems bad, based on the previous one
			if (!stateQueue.empty())
			{
				const auto da = 1000.f * (stateQueue.back().acc - state.acc);
				const float crazyAcc = 400.f;
				if (fabs(da.x) > crazyAcc || fabs(da.y) > crazyAcc || fabs(da.z) > crazyAcc)
				{
					std::cout << "Bad frame!\n";
					continue;
				}
			}

			stateQueue.push_back(state);

			tempGraph.Update(state.temp);
			presGraph.Update(state.pressure);
			rollGraph.Update(glm::degrees(roll(state.quat)));
			pitchGraph.Update(glm::degrees(yaw(state.quat)));
			yawGraph.Update(glm::degrees(pitch(state.quat)));
			accXGraph.Update(state.acc.x * 1000.f);
			accYGraph.Update(state.acc.y * 1000.f);
			accZGraph.Update(state.acc.z * 1000.f);

			lastCan = state;
			realState = state;
		}

		// Remove from buffer the processed bytes, leave a stub to which new data will be added on the next update
		serialBuffer.erase(serialBuffer.begin(), serialBuffer.begin() + toRemove);

		// Add the latest batch of states to the timeline
		// TODO changed toggled recording to always recording 
		auto newStates = GetCanState();
		if (!newStates.empty())
		{
			size_t stateIdx = 0;
			if (!states.empty())
				stateIdx = newStates[0].timestamp == states.back().timestamp ? 1 : 0;
			for (; stateIdx < newStates.size(); ++stateIdx)
				states.push_back(newStates[stateIdx]);

			auto& timeline = timelines.timelines[timelines.selected];
			if (timelines.state == TimelineMgr::State::RECORDING || timelines.state == TimelineMgr::State::INACTIVE)
				for (auto&& state : newStates)
					timelines.AddState(state);
			//timelines.RunIntegrator();
			realState = timeline.states.back();
		}

		if (!states.empty())
		{
			states.erase(states.begin());
		}
	}
};

std::vector<DataContext> dataContexts;
int activeContext = -1;

float basePressure = 995.9f;//990.f;

void FrameStart()
{
    const auto currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    perf.Reset();            // Profiling
    inputState.Update();     // Mouse/keyboard input
    fbState.Update();        // Window resizing 
    timerMgr.Tick();         // Timinng

	// Update all contexts, even if they are not active
	for (auto& context : dataContexts)
		context.Update();
}

float altitude = 0.f;

void DrawUI()
{
    vg->BeginFrame(fbState.winWidth, fbState.winHeight, fbState.pxRatio);
    
    if (!uiInit)
    {
        InitUI();
    }

    // Update ui state
    if (connectedPort != 0)
        serialConnectBtn->text = "Disconnect " + std::to_string(connectedPort);
    else
        serialConnectBtn->text = "Connect " + std::to_string(curInputPort);

    // Render ui state 
    // Timeline bg 
    vg->BeginPath();
    vg->CreateRect(0.f, fbState.winHeight * 0.85f, fbState.winWidth, fbState.winHeight * 0.15f);
    vg->FillColor(nvg::RGBA(0, 0, 0, 128));
    vg->Fill();
    
    vg->Save();
        
    for (auto element : screen.elements)
    {
        element->UpdateScale(fbState.winWidth, fbState.winHeight);
        element->Render(*vg);
    }
        
    vg->Restore();

    if (timelines.state == TimelineMgr::State::PLAYING)
    {
        vg->SetFontSize(40);
		vg->SetFontFace(vis::fonts::GetFontByName("sans"));
        vg->FillColor(nvg::RGBA(255, 0, 0, 255));
        vg->TextAlign(nvg::Align::LEFT | nvg::Align::MIDDLE);
        //vg->Text(450, 550, "PLAYBACK", nullptr);

        auto playbackDt = std::chrono::duration<double, std::milli>(std::chrono::steady_clock::now() - timelines.playbackStart).count();
        std::string dtLabel = std::string("+") + std::to_string((int)playbackDt / 1000) + "." + std::to_string((int)playbackDt % 1000) + " s";

        vg->SetFontSize(20);
		vg->SetFontFace(vis::fonts::GetFontByName("sans"));
        vg->FillColor(nvg::RGBA(255, 0, 0, 255));
        vg->TextAlign(nvg::Align::LEFT | nvg::Align::MIDDLE);
        vg->Text(fbState.winWidth - 75, 500, dtLabel.c_str(), NULL);
    }

    //perf.Render(vg);

	if (timelines.state == TimelineMgr::State::PLAYING || timelines.state == TimelineMgr::State::PAUSED)
	{
		graphTemp.tempGraph.Render(*vg, 5, 50);
		graphTemp.presGraph.Render(*vg, 5, 125);
		graphTemp.rollGraph.Render(*vg, 5, 200);
		graphTemp.pitchGraph.Render(*vg, 5, 300);
		graphTemp.yawGraph.Render(*vg, 5, 400);
		graphTemp.accXGraph.Render(*vg, 600, 200);
		graphTemp.accYGraph.Render(*vg, 600, 300);
		graphTemp.accZGraph.Render(*vg, 600, 400);
	}
	else
	{
		tempGraph.Render(*vg, 5, 50);
		presGraph.Render(*vg, 5, 125);
		rollGraph.Render(*vg, 5, 200);
		pitchGraph.Render(*vg, 5, 300);
		yawGraph.Render(*vg, 5, 400);
		accXGraph.Render(*vg, 600, 200);
		accYGraph.Render(*vg, 600, 300);
		accZGraph.Render(*vg, 600, 400);
	}

	// Altitude
	if (timelines.timelines[timelines.selected].states.size())
	{
		vg->SetFontSize(20);
		vg->SetFontFace(vis::fonts::GetFontByName("sans"));
		vg->FillColor(nvg::RGBA(255, 255, 255, 255));
		vg->TextAlign(nvg::Align::LEFT | nvg::Align::MIDDLE);
		char buff[30];
		sprintf(buff, "Altitude: %.2fm", altitude);
		vg->Text(600, 100, buff, nullptr);
	}

    vg->EndFrame();
}

CanState PickCanToDraw()
{
    CanState result;
    switch (timelines.state)
    {
    case TimelineMgr::State::RECORDING:
    case TimelineMgr::State::INACTIVE:
        timerMgr.Pause(timelineTimer);
        result = realState;
        //std::cout << "POS: " << result.pos.x << '\n';
        break;
    case TimelineMgr::State::PAUSED:
        timelines.currentFrame = timelines.timelines[timelines.selected].states.size() * slider->knobPos;
        result = timelines.timelines[timelines.selected].states[timelines.currentFrame];

		{
			graphTemp = GraphTemp();
			
			for (size_t i = 1; i <= timelines.currentFrame; ++i)
			{
				auto frame = timelines.timelines[timelines.selected].states[i];
				graphTemp.tempGraph.Update(frame.temp);
				graphTemp.presGraph.Update(frame.pressure);
				graphTemp.rollGraph.Update(glm::degrees(roll(frame.quat)));
				graphTemp.pitchGraph.Update(glm::degrees(yaw(frame.quat)));
				graphTemp.yawGraph.Update(glm::degrees(pitch(frame.quat)));
				graphTemp.accXGraph.Update(frame.acc.x * 1000.f);
				graphTemp.accYGraph.Update(frame.acc.y * 1000.f);
				graphTemp.accZGraph.Update(frame.acc.z * 1000.f);
			}
		}

        break;
    case TimelineMgr::State::PLAYING:
        timerMgr.Resume(timelineTimer);
        if (timelines.currentFrame < timelines.timelines[timelines.selected].states.size())
        {
            if (timerMgr.IsReady(timelineTimer))
            {
                if (timelines.currentFrame < timelines.timelines[timelines.selected].states.size() - 1)
                    timerMgr.SetTimer(timelineTimer, 10000 * std::chrono::duration<double, std::milli>(timelines.timelines[timelines.selected].states[timelines.currentFrame + 1].timestamp - timelines.timelines[timelines.selected].states[timelines.currentFrame].timestamp).count());
                else
                {
                    timelines.state = TimelineMgr::State::PAUSED;
                    std::cout << "Finished playing\n";
                    realState = timelines.timelines[timelines.selected].states[timelines.currentFrame];
                }

                slider->knobPos = float(timelines.currentFrame) / float(timelines.timelines[timelines.selected].states.size());

                result = timelines.timelines[timelines.selected].states[timelines.currentFrame++];

				graphTemp.tempGraph.Update(result.temp);
				graphTemp.presGraph.Update(result.pressure);
				graphTemp.rollGraph.Update(glm::degrees(roll(result.quat)));
				graphTemp.pitchGraph.Update(glm::degrees(yaw(result.quat)));
				graphTemp.yawGraph.Update(glm::degrees(pitch(result.quat)));
				graphTemp.accXGraph.Update(result.acc.x * 1000.f);
				graphTemp.accYGraph.Update(result.acc.y * 1000.f);
				graphTemp.accZGraph.Update(result.acc.z * 1000.f);
            }
        }
        break;
    }

    return result;
}

GLuint lineVAO = 0;
size_t indexCountGrid = 0;
Shader* gridShader;
float lineLength = 25.f;
float spacing = .01f;
int numLines = 50;

void DrawAxis(const glm::vec3& axis, Shader* shader, float angle = 0.f)
{
    glm::mat4 model;

    if (angle > 1.f)
        model = glm::rotate(model, angle, axis);

    model = glm::translate(model, glm::vec3(0 - numLines / 2 * spacing, 0.0f, 0.0f));

    // Along Z
    for (int line = 0; line < numLines; line++)
    {
        shader->SetMat4(shader->GetUniformLoc("model"), model);
        gl::DrawArrays(gl::LINES, 0, 2);

        model = glm::translate(model, glm::vec3(spacing, 0.0f, 0.0f));
    }
}

void DrawGrid(const glm::vec2& winExtent)
{
    if (lineVAO == 0)
    {
        gl::GenVertexArrays(1, &lineVAO);
        GLuint vbo;
        gl::GenBuffers(1, &vbo);

        gl::BindVertexArray(lineVAO);

        float vertices[] = {
            -lineLength,
            lineLength
        };

        gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
        gl::BufferData(gl::ARRAY_BUFFER, 2 * sizeof(float), vertices, gl::STATIC_DRAW);

        gl::EnableVertexAttribArray(0);
        gl::VertexAttribPointer(0, 1, gl::FLOAT, gl::FALSE_, sizeof(float), (GLvoid*)(0));

        gl::BindVertexArray(0);
    }

    gl::Enable(gl::DEPTH_TEST);

    gridShader->Use();
    gridShader->SetMat4(gridShader->GetUniformLoc("view"), camera.GetViewMatrix());
    const auto projection = glm::perspective(glm::radians(45.0f), winExtent.x / winExtent.y, 0.1f, 100.0f);
    gridShader->SetMat4(gridShader->GetUniformLoc("projection"), projection);

    gl::BindVertexArray(lineVAO);

    // Z-axis
    DrawAxis(glm::vec3(0.0f), gridShader, 0.f);
    // X-axis
    DrawAxis(glm::vec3(0.0f, 1.0f, 0.0f), gridShader, glm::radians(90.0f));

    gl::DrawElements(gl::TRIANGLE_STRIP, indexCountGrid, gl::UNSIGNED_INT, 0);
    gl::BindVertexArray(0);
}

GLuint pathVAO = 0;
size_t indexCountPath = 0;

void DrawPath(const glm::vec2& extent)
{
    if (pathVAO == 0)
    {
        gl::GenVertexArrays(1, &pathVAO);
        GLuint vbo;
        gl::GenBuffers(1, &vbo);

        gl::BindVertexArray(pathVAO);

        float vertices[] = {
            -lineLength,
            lineLength
        };

        gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
        gl::BufferData(gl::ARRAY_BUFFER, 2 * sizeof(float), vertices, gl::STATIC_DRAW);

        gl::EnableVertexAttribArray(0);
        gl::VertexAttribPointer(0, 1, gl::FLOAT, gl::FALSE_, sizeof(float), (GLvoid*)(0));

        gl::BindVertexArray(0);
    }
}

void DrawEvt(const glm::vec2& winExtent)
{
    //DrawGrid(winExtent);
    //DrawPath(winExtent);
}

struct ShaderHelper
{
    virtual ~ShaderHelper() = default;
    virtual void DrawStart(const glm::vec2& winExtent) const = 0;
    virtual void Draw(std::shared_ptr<Model> mesh, const glm::vec3& pos, const glm::quat& orient) const = 0;
};

struct PhongShader : ShaderHelper
{
    PhongShader() : shader("phong")
    {
        projLoc = shader.GetUniformLoc("projection");
        viewLoc = shader.GetUniformLoc("view");
        modelLoc = shader.GetUniformLoc("model");
    }

    void DrawStart(const glm::vec2& winExtent) const override
    {
        gl::Enable(gl::DEPTH_TEST);
        shader.Use();

        shader.SetMat4(viewLoc, camera.GetViewMatrix());
        
        const auto projection = glm::perspective(glm::radians(45.0f), winExtent.x / winExtent.y, 0.1f, 100.0f);
        shader.SetMat4(projLoc, projection);
    }

    void Draw(std::shared_ptr<Model> mesh, const glm::vec3& pos, const glm::quat& orient) const override
    {
        glm::mat4 model;
        model = rotate(model, pitch(orient), { 0.f, 1.f, 0.f });
        model = rotate(model, yaw(orient), { 1.f, 0.f, 0.f });
        model = rotate(model, roll(orient), { 0.f, 0.f, 1.f });
        model = translate(model, pos);
        shader.SetMat4(modelLoc, model);

        mesh->Draw(shader.program);
    }
    
private:
    Shader shader;
    GLint projLoc, viewLoc, modelLoc;
};

unsigned int sphereVAO = 0;
unsigned int indexCount;
void DrawSphere()
{
    if (sphereVAO == 0)
    {
        gl::GenVertexArrays(1, &sphereVAO);

        unsigned int vbo, ebo;
        gl::GenBuffers(1, &vbo);
        gl::GenBuffers(1, &ebo);

        std::vector<glm::vec3> positions;
        std::vector<glm::vec2> uv;
        std::vector<glm::vec3> normals;
        std::vector<unsigned int> indices;

        const unsigned int X_SEGMENTS = 64;
        const unsigned int Y_SEGMENTS = 64;
        const float PI = 3.14159265359;
        for (unsigned int y = 0; y <= Y_SEGMENTS; ++y)
        {
            for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
            {
                float xSegment = (float)x / (float)X_SEGMENTS;
                float ySegment = (float)y / (float)Y_SEGMENTS;
                float xPos = cosf(xSegment * 2.0f * PI) * sinf(ySegment * PI);
                float yPos = cosf(ySegment * PI);
                float zPos = sinf(xSegment * 2.0f * PI) * sinf(ySegment * PI);

                positions.push_back(glm::vec3(xPos, yPos, zPos));
                uv.push_back(glm::vec2(xSegment, ySegment));
                normals.push_back(glm::vec3(xPos, yPos, zPos));
            }
        }

        bool oddRow = false;
        for (int y = 0; y < Y_SEGMENTS; ++y)
        {
            if (!oddRow) // even rows: y == 0, y == 2; and so on
            {
                for (int x = 0; x <= X_SEGMENTS; ++x)
                {
                    indices.push_back(y       * (X_SEGMENTS + 1) + x);
                    indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
                }
            }
            else
            {
                for (int x = X_SEGMENTS; x >= 0; --x)
                {
                    indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
                    indices.push_back(y       * (X_SEGMENTS + 1) + x);
                }
            }
            oddRow = !oddRow;
        }
        indexCount = indices.size();

        std::vector<float> data;
        for (int i = 0; i < positions.size(); ++i)
        {
            data.push_back(positions[i].x);
            data.push_back(positions[i].y);
            data.push_back(positions[i].z);
            if (uv.size() > 0)
            {
                data.push_back(uv[i].x);
                data.push_back(uv[i].y);
            }
            if (normals.size() > 0)
            {
                data.push_back(normals[i].x);
                data.push_back(normals[i].y);
                data.push_back(normals[i].z);
            }
        }
        gl::BindVertexArray(sphereVAO);
        gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
        gl::BufferData(gl::ARRAY_BUFFER, data.size() * sizeof(float), &data[0], gl::STATIC_DRAW);
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ebo);
        gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], gl::STATIC_DRAW);
        float stride = (3 + 2 + 3) * sizeof(float);
        gl::EnableVertexAttribArray(0);
        gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE_, stride, (void*)0);
        gl::EnableVertexAttribArray(1);
        gl::VertexAttribPointer(1, 2, gl::FLOAT, gl::FALSE_, stride, (void*)(3 * sizeof(float)));
        gl::EnableVertexAttribArray(2);
        gl::VertexAttribPointer(2, 3, gl::FLOAT, gl::FALSE_, stride, (void*)(5 * sizeof(float)));
    }

    gl::BindVertexArray(sphereVAO);
    gl::DrawElements(gl::TRIANGLE_STRIP, indexCount, gl::UNSIGNED_INT, 0);
}

glm::vec3 lightPositions[] = {
    glm::vec3(-10.0f,  10.0f, 10.0f),
    glm::vec3(10.0f,  10.0f, 10.0f),
    glm::vec3(-10.0f, -10.0f, 10.0f),
    glm::vec3(10.0f, -10.0f, 10.0f),
};

struct PbrShader : ShaderHelper
{
    Shader shader;

    PbrShader() : shader("pbr")
    {
    }

    void DrawStart(const glm::vec2& winExtent) const override
    {
        gl::Enable(gl::DEPTH_TEST);
        shader.Use();

        shader.SetMat4("view", camera.GetViewMatrix());
        shader.SetVec3("camPos", camera.Position);

        const auto projection = glm::perspective(glm::radians(45.0f), winExtent.x / winExtent.y, 0.1f, 100.0f);
        shader.SetMat4("projection", projection);
    }

    void Draw(std::shared_ptr<Model> mesh, const glm::vec3& pos, const glm::quat& orient) const override
    {
        shader.SetVec3("albedo", { 1.f, 1.f, 0.0f });
        shader.SetFloat("ao", 1.0f);

        shader.SetFloat("metallic", .5f);
        shader.SetFloat("roughness", 0.7f);

        glm::mat4 model;
        model = translate(model, pos);
        model = rotate(model, pitch(orient), { 0.f, 1.f, 0.f });
        model = rotate(model, yaw(orient), { 1.f, 0.f, 0.f });
        model = rotate(model, roll(orient), { 0.f, 0.f, 1.f });
        shader.SetMat4("model", model);

        mesh->Draw(shader.program);

        for (unsigned int i = 0; i < sizeof(lightPositions) / sizeof(lightPositions[0]); ++i)
        {
            shader.SetVec3("lightPositions[" + std::to_string(i) + "]", lightPositions[i]);
            shader.SetVec3("lightColors[" + std::to_string(i) + "]", { 255.f, 255.f, 255.f });

            model = glm::mat4();
            model = translate(model, lightPositions[i]);
            model = scale(model, glm::vec3(0.5f));
            shader.SetMat4("model", model);
            DrawSphere();
        }
    }
};

ShaderHelper* shader;

void FrameRender()
{
    gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT | gl::STENCIL_BUFFER_BIT);

	DrawEvt({ fbState.fbWidth, fbState.winHeight });
	shader->DrawStart({ fbState.winWidth, fbState.winHeight });

	//if (dataContexts[activeContext].m_source == DataContext::Source::NONE)
	//{
	//	// Choose source
	//}
	//else
	//{
	//	const auto can = PickCanToDraw();
	//	auto mesh = *vis::meshes::GetMesh("assets/can.obj");
	//	shader->Draw(mesh, can.pos, can.quat);
	//	DrawUI();
	//}
	const auto can = PickCanToDraw();
	auto mesh = *vis::meshes::GetMesh("assets/can.obj");
	shader->Draw(mesh, can.pos, can.quat);
	auto estAlt = EstimateAltitude(can.pressure, basePressure);
	//altitude =  estAlt > 0.f ? estAlt : 0.f;
	altitude = estAlt;
	DrawUI();

	// Launch location elevation: 50m
}

void FrameEnd()
{
    perf.Elapsed();
}

void VisWindow::Loop()
{
    shader = new PbrShader();
    gridShader = new Shader("grid");

	dataContexts.push_back(DataContext());
	activeContext = 0;

	while (!glfwWindowShouldClose(m_window))
	{
		FrameStart();
        FrameRender();
        FrameEnd();

		glfwSwapBuffers(m_window);
		glfwPollEvents();
	}
}

void VisWindow::CopyResources()
{
	vg->fs->fonts.push_back(static_cast<fons::Font*>(vis::fonts::GetFont(vis::fonts::GetFontByName("sans"))));
	vg->fs->fonts.push_back(static_cast<fons::Font*>(vis::fonts::GetFont(vis::fonts::GetFontByName("sans-bold"))));
}
