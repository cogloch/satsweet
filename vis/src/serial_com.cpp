#include "stdafx.h"
#include "serial_com.h"


SerialCom::SerialCom(const int comPortNum, const int baudRate)
    : m_handle(nullptr)
    , m_comPortNum(comPortNum)
    , m_baudRate(baudRate)
{
    if (comPortNum < 1)
        throw "SerialCom::SerialCom: Invalid COM port number";

    std::string portName;
    portName = R"(\\.\COM)" + std::to_string(comPortNum);

    m_handle = CreateFileA(portName.c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);

    if (m_handle == INVALID_HANDLE_VALUE)
        throw "SerialCom::SerialCom: Opening serial port failed";

    std::cout << "Serial port " << comPortNum << " open.\n";

    DCB dcbSerialParams = {};
    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
    GetCommState(m_handle, &dcbSerialParams);
    dcbSerialParams.BaudRate = m_baudRate;
    dcbSerialParams.ByteSize = 8;
    dcbSerialParams.StopBits = ONESTOPBIT;
    dcbSerialParams.Parity = NOPARITY;
    dcbSerialParams.EvtChar = '$';
    SetCommState(m_handle, &dcbSerialParams);

    COMMTIMEOUTS timeouts = {};
    timeouts.ReadIntervalTimeout = 50;
    timeouts.ReadTotalTimeoutConstant = 50;
    timeouts.ReadTotalTimeoutMultiplier = 10;
    timeouts.WriteTotalTimeoutConstant = 50;
    timeouts.WriteTotalTimeoutMultiplier = 10;
    SetCommTimeouts(m_handle, &timeouts);
}

SerialCom::~SerialCom()
{
    CloseHandle(m_handle);
}

void SerialCom::Write(const std::string& string)
{
    WriteFile(m_handle, string.c_str(), string.length(), nullptr, nullptr);
}

void SerialCom::Listen()
{
    SetCommMask(m_handle, EV_RXCHAR); 

    for (;;)
    {
        unsigned long event;
        WaitCommEvent(m_handle, &event, nullptr);

        DWORD hasRead = 0;
        const size_t tempBufSz = 128;
        std::vector<u8> tempBuffer(tempBufSz); 
        size_t curRead = 0;
        for(;;)
        {
            ReadFile(m_handle, &tempBuffer[curRead], 1, &hasRead, nullptr);
            if (!hasRead) break;

            // Temp buffer full, dump to main buffer
            if (curRead == tempBufSz - 1)
            {
                m_listenMtx.lock();
                m_bytes.insert(m_bytes.end(), tempBuffer.begin(), tempBuffer.end());
                m_listenMtx.unlock();

                curRead = 0;
            }
            
            curRead++;
        }
        
        m_listenMtx.lock();
        m_bytes.insert(m_bytes.end(), tempBuffer.begin(), tempBuffer.begin() + curRead);
        m_listenMtx.unlock();
    }
}

std::vector<u8> SerialCom::GetData()
{
    m_listenMtx.lock();
    auto result = m_bytes;
    m_bytes.clear();
    m_listenMtx.unlock();
    return result;
}
