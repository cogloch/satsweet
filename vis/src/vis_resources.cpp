#include "stdafx.h"
#include "vis_resources.h"
#include <map>
#include <array>

// TODO move out stb libs 
#include "nanovg\stb_truetype.h"

// TODO temp
#include "nanovg\fontstash.h"
#include "nanovg\nanovg.h"
//#include "nanovg\fontstash.h"

namespace vis
{
	struct Data
	{
		u8* bytes;
		size_t sz;
	};

	const int INVALID_RES = -1;
	nvg::Context* g_nvcontext;

	namespace fonts
	{
		const int SCRATCH_BUF_SIZE = 64000;
		const int HASH_LUT_SIZE = 256;
		const int INIT_FONTS = 4;
		const int INIT_GLYPHS = 256;
		const int INIT_ATLAS_NODES = 256;
		const int VERTEX_COUNT = 1024;
		const int MAX_STATES = 20;
		const int MAX_FALLBACKS = 20;

		// Internal classes 
		struct Glyph
		{
			unsigned int codepoint;
			int index;
			int next;
			short size, blur;
			short x0, y0, x1, y1;
			short xadv, xoff, yoff;
		};

		struct Font
		{
			Font();
			~Font();

			stbtt_fontinfo font;
			std::string name;
			u8* data;
			int dataSize;
			u8 freeData;
			float ascender;
			float descender;
			float lineh;
			Glyph* glyphs;
			int cglyphs;
			int nglyphs;
			std::array<int, HASH_LUT_SIZE> lut;
			std::array<int, MAX_FALLBACKS> fallbacks;
			int nfallbacks;
		};

		// Internal state 
		std::vector<Font*> s_fonts;
		int nscratch = 0;
		
		// Internal funcs
		Font::Font()
			: font()
			, data(nullptr)
			, dataSize(0)
			, freeData(0)
			, ascender(0.f)
			, descender(0.f)
			, lineh(0.f)
			, glyphs(nullptr), cglyphs(INIT_GLYPHS), nglyphs(0)
			, lut()
			, fallbacks()
			, nfallbacks(0)
		{
			glyphs = new Glyph[INIT_GLYPHS];

			// Init hash lookup.
			memset(&lut, -1, sizeof(int) * HASH_LUT_SIZE);
			memset(&fallbacks, 0, sizeof(int) * MAX_FALLBACKS);
		}

		Font::~Font()
		{
			delete glyphs;
			if (freeData && data) delete data;
		}

		// Public interface 
		int GetFontByName(const std::string& name)
		{
			for (int i = 0, numFonts = s_fonts.size(); i < numFonts; ++i)
				if (name == s_fonts[i]->name)
					return i;

			return FONS_INVALID;
		}

		void* GetFont(int id)
		{
			return s_fonts[id];
		}

		int LoadFromFile(const std::string & path, const std::string& name)
		{
			std::ifstream file(path, std::ios::binary);
			file.seekg(0, std::ifstream::end);
			Data data;
			data.sz = static_cast<size_t>(file.tellg());
			file.seekg(0, std::istream::beg);
			data.bytes = new u8[data.sz];
			file.read(reinterpret_cast<char*>(data.bytes), data.sz);

			return LoadFromMem(name, data.bytes, data.sz, 1);
		}

		int LoadFromMem(const std::string& name, u8* data, size_t dataSz, size_t freeData)
		{
			s_fonts.push_back(new Font());
			const int idx = s_fonts.size() - 1;
			
			Font& font = *s_fonts[idx];
			font.name = name;

			// Read in the font data.
			font.data = data;
			font.dataSize = dataSz;
			font.freeData = static_cast<u8>(freeData);

			// Init font
			nscratch = 0;
			
			font.font.userdata = g_nvcontext->fs;
			int stbOk = stbtt_InitFont(&font.font, data, 0);
			if (!stbOk)
			{
				delete &font;
				s_fonts.pop_back();
				return INVALID_RES;
			}

			// Store normalized line height. The real line height is got by multiplying the lineh by font size.
			int ascent, descent, lineGap;
			stbtt_GetFontVMetrics(&font.font, &ascent, &descent, &lineGap);
			int fh = ascent - descent;
			font.ascender = (float)ascent / (float)fh;
			font.descender = (float)descent / (float)fh;
			font.lineh = (float)(fh + lineGap) / (float)fh;

			return idx;
		}
	}

	namespace meshes
	{
		// Internal state 
		std::vector<std::shared_ptr<Model>> models;
		std::map<std::string, size_t> modelPaths;

		// Public interface 
		std::shared_ptr<Model> LoadMesh(const std::string& path)
		{
			models.push_back(std::make_shared<Model>(path));
			modelPaths.insert(std::make_pair(path, models.size() - 1));
			return models.back();
		}

		std::optional<std::shared_ptr<Model>> GetMesh(const std::string& path)
		{
			auto it = modelPaths.find(path);
			if (it == modelPaths.end())
				return {};
			return models[it->second];
		}
	}
}
