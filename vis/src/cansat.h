#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include "model.h"
#include <chrono>


struct CanState
{
    CanState()
    {
        temp = 0.0;
        pressure = 0.0;
        timestamp = 0;
    }

    unsigned long timestamp;
    glm::vec3 pos;
    glm::vec3 vel;
    glm::vec3 acc;
    glm::quat quat;
    float temp;        // In deg C
    float pressure;    // Absolute pressure, in mb 

    static int readings;
    static glm::vec3 baseAcc;
    static const glm::vec3 invalidVec;

    bool dirty = false;
};

struct Cansat
{
    void Init();
    void Draw(glm::vec2 winExtent, CanState& state, Model& mesh);
};

struct Timeline
{
    std::vector<CanState> states;
};

struct TimelineMgr
{
    TimelineMgr()
    {
        // TODO temp
        selected = AddTimeline();
    }

    std::vector<Timeline> timelines;
    size_t selected = 0;

    size_t AddTimeline();
    void AddState(CanState);
    void RunIntegrator();
    size_t LoadTimeline(const std::wstring& name); 
    void DumpTimeline(size_t id);                   // Path will be resolved automatically to dumps/date_time - a raw file date_time and date_time.xlsx
    void SelectTimeline(size_t id);
    void DeleteTimeline(size_t id);

    void OnExport();
    void OnRecordKey();
    void OnPlayKey();
    void OnLoad();
    void JumpTo(float point); // Jump to point in timeline, as a percentage (0..1)

    // May record what's being done IRL while playing a previous timeline (TEMP: NOT!)
    enum class State
    {
        INACTIVE,    // The real state of the can is being displayed (but not recording it) 
        RECORDING,   // Recording a new timeline from cansat 
        PLAYING,     // Playing an existing timeline 
        PAUSED       // Playing an existing timeline (paused at some frame) 
    } state = State::INACTIVE;
    size_t currentFrame = 0;
    std::chrono::steady_clock::time_point playbackStart;
};
