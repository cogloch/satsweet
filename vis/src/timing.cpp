#include "stdafx.h"
#include "timing.h"


TimerMgr::TimerMgr()
{
    prevUpdate = std::chrono::steady_clock::now();
}

TimerHandle TimerMgr::AddTimer(const double timeout)
{
    timers.push_back({ timeout, timeout, true });
    return timers.size() - 1;
}

void TimerMgr::SetTimer(const TimerHandle handle, const double timeout)
{
    timers[handle] = { timeout, timeout, true };
}

bool TimerMgr::IsReady(const TimerHandle handle)
{
    bool ready = timers[handle].currentTimeout <= 0.0;
    timers[handle].currentTimeout = timers[handle].timeout;
    return ready;
}

void TimerMgr::Tick()
{
    const auto now = std::chrono::steady_clock::now();
    const double dt = (now - prevUpdate).count();
    prevUpdate = now;

    for (auto& timer : timers)
        if(timer.running)
            timer.currentTimeout -= dt;
}

void TimerMgr::Pause(const TimerHandle handle)
{
    timers[handle].running = false;
}

void TimerMgr::Resume(const TimerHandle handle)
{
    timers[handle].running = true;
}