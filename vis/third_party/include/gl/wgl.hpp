#ifndef WINDOWSGL_NOLOAD_STYLE_HPP
#define WINDOWSGL_NOLOAD_STYLE_HPP

#ifdef __wglext_h_
#error Attempt to include auto-generated WGL header after wglext.h
#endif

#define __wglext_h_

#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN 1
#endif
#ifndef NOMINMAX
	#define NOMINMAX
#endif
#include <windows.h>

#ifdef CODEGEN_FUNCPTR
#undef CODEGEN_FUNCPTR
#endif /*CODEGEN_FUNCPTR*/
#define CODEGEN_FUNCPTR WINAPI

#ifndef GL_LOAD_GEN_BASIC_OPENGL_TYPEDEFS
#define GL_LOAD_GEN_BASIC_OPENGL_TYPEDEFS

typedef unsigned int GLenum;
typedef unsigned char GLboolean;
typedef unsigned int GLbitfield;
typedef signed char GLbyte;
typedef short GLshort;
typedef int GLint;
typedef int GLsizei;
typedef unsigned char GLubyte;
typedef unsigned short GLushort;
typedef unsigned int GLuint;
typedef float GLfloat;
typedef float GLclampf;
typedef double GLdouble;
typedef double GLclampd;
#define GLvoid void

#endif /*GL_LOAD_GEN_BASIC_OPENGL_TYPEDEFS*/


#ifndef GL_LOAD_GEN_BASIC_OPENGL_TYPEDEFS
#define GL_LOAD_GEN_BASIC_OPENGL_TYPEDEFS


#endif /*GL_LOAD_GEN_BASIC_OPENGL_TYPEDEFS*/

struct _GPU_DEVICE {
    DWORD  cb;
    CHAR   DeviceName[32];
    CHAR   DeviceString[128];
    DWORD  Flags;
    RECT   rcVirtualScreen;
};
DECLARE_HANDLE(HPBUFFERARB);
DECLARE_HANDLE(HPBUFFEREXT);
DECLARE_HANDLE(HVIDEOOUTPUTDEVICENV);
DECLARE_HANDLE(HPVIDEODEV);
DECLARE_HANDLE(HGPUNV);
DECLARE_HANDLE(HVIDEOINPUTDEVICENV);
typedef struct _GPU_DEVICE *PGPU_DEVICE;

namespace wgl
{
	/////////////////////////
	// Extension Variables
	namespace exts
	{
		extern bool var_ARB_multisample;
		extern bool var_ARB_extensions_string;
		extern bool var_ARB_pixel_format;
		extern bool var_ARB_pixel_format_float;
		extern bool var_ARB_framebuffer_sRGB;
		extern bool var_ARB_create_context;
		extern bool var_ARB_create_context_profile;
		extern bool var_ARB_create_context_robustness;
		extern bool var_EXT_swap_control;
		extern bool var_EXT_pixel_format_packed_float;
		extern bool var_EXT_create_context_es2_profile;
		extern bool var_EXT_swap_control_tear;
		extern bool var_NV_swap_group;
	}
	
	enum
	{
		// Extension: ARB_multisample
		SAMPLES_ARB                      = 0x2042,
		SAMPLE_BUFFERS_ARB               = 0x2041,
		
		// Extension: ARB_pixel_format
		ACCELERATION_ARB                 = 0x2003,
		ACCUM_ALPHA_BITS_ARB             = 0x2021,
		ACCUM_BITS_ARB                   = 0x201D,
		ACCUM_BLUE_BITS_ARB              = 0x2020,
		ACCUM_GREEN_BITS_ARB             = 0x201F,
		ACCUM_RED_BITS_ARB               = 0x201E,
		ALPHA_BITS_ARB                   = 0x201B,
		ALPHA_SHIFT_ARB                  = 0x201C,
		AUX_BUFFERS_ARB                  = 0x2024,
		BLUE_BITS_ARB                    = 0x2019,
		BLUE_SHIFT_ARB                   = 0x201A,
		COLOR_BITS_ARB                   = 0x2014,
		DEPTH_BITS_ARB                   = 0x2022,
		DOUBLE_BUFFER_ARB                = 0x2011,
		DRAW_TO_BITMAP_ARB               = 0x2002,
		DRAW_TO_WINDOW_ARB               = 0x2001,
		FULL_ACCELERATION_ARB            = 0x2027,
		GENERIC_ACCELERATION_ARB         = 0x2026,
		GREEN_BITS_ARB                   = 0x2017,
		GREEN_SHIFT_ARB                  = 0x2018,
		NEED_PALETTE_ARB                 = 0x2004,
		NEED_SYSTEM_PALETTE_ARB          = 0x2005,
		NO_ACCELERATION_ARB              = 0x2025,
		NUMBER_OVERLAYS_ARB              = 0x2008,
		NUMBER_PIXEL_FORMATS_ARB         = 0x2000,
		NUMBER_UNDERLAYS_ARB             = 0x2009,
		PIXEL_TYPE_ARB                   = 0x2013,
		RED_BITS_ARB                     = 0x2015,
		RED_SHIFT_ARB                    = 0x2016,
		SHARE_ACCUM_ARB                  = 0x200E,
		SHARE_DEPTH_ARB                  = 0x200C,
		SHARE_STENCIL_ARB                = 0x200D,
		STENCIL_BITS_ARB                 = 0x2023,
		STEREO_ARB                       = 0x2012,
		SUPPORT_GDI_ARB                  = 0x200F,
		SUPPORT_OPENGL_ARB               = 0x2010,
		SWAP_COPY_ARB                    = 0x2029,
		SWAP_EXCHANGE_ARB                = 0x2028,
		SWAP_LAYER_BUFFERS_ARB           = 0x2006,
		SWAP_METHOD_ARB                  = 0x2007,
		SWAP_UNDEFINED_ARB               = 0x202A,
		TRANSPARENT_ALPHA_VALUE_ARB      = 0x203A,
		TRANSPARENT_ARB                  = 0x200A,
		TRANSPARENT_BLUE_VALUE_ARB       = 0x2039,
		TRANSPARENT_GREEN_VALUE_ARB      = 0x2038,
		TRANSPARENT_INDEX_VALUE_ARB      = 0x203B,
		TRANSPARENT_RED_VALUE_ARB        = 0x2037,
		TYPE_COLORINDEX_ARB              = 0x202C,
		TYPE_RGBA_ARB                    = 0x202B,
		
		// Extension: ARB_pixel_format_float
		TYPE_RGBA_FLOAT_ARB              = 0x21A0,
		
		// Extension: ARB_framebuffer_sRGB
		FRAMEBUFFER_SRGB_CAPABLE_ARB     = 0x20A9,
		
		// Extension: ARB_create_context
		CONTEXT_DEBUG_BIT_ARB            = 0x00000001,
		CONTEXT_FLAGS_ARB                = 0x2094,
		CONTEXT_FORWARD_COMPATIBLE_BIT_ARB = 0x00000002,
		CONTEXT_LAYER_PLANE_ARB          = 0x2093,
		CONTEXT_MAJOR_VERSION_ARB        = 0x2091,
		CONTEXT_MINOR_VERSION_ARB        = 0x2092,
		ERROR_INVALID_VERSION_ARB        = 0x2095,
		
		// Extension: ARB_create_context_profile
		CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB = 0x00000002,
		CONTEXT_CORE_PROFILE_BIT_ARB     = 0x00000001,
		CONTEXT_PROFILE_MASK_ARB         = 0x9126,
		ERROR_INVALID_PROFILE_ARB        = 0x2096,
		
		// Extension: ARB_create_context_robustness
		CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB = 0x8256,
		CONTEXT_ROBUST_ACCESS_BIT_ARB    = 0x00000004,
		LOSE_CONTEXT_ON_RESET_ARB        = 0x8252,
		NO_RESET_NOTIFICATION_ARB        = 0x8261,
		
		// Extension: EXT_pixel_format_packed_float
		TYPE_RGBA_UNSIGNED_FLOAT_EXT     = 0x20A8,
		
		// Extension: EXT_create_context_es2_profile
		CONTEXT_ES2_PROFILE_BIT_EXT      = 0x00000004,
		
	};
	
	// Extension: ARB_extensions_string
	extern const char * (CODEGEN_FUNCPTR *GetExtensionsStringARB)(HDC hdc);
	
	// Extension: ARB_pixel_format
	extern BOOL (CODEGEN_FUNCPTR *ChoosePixelFormatARB)(HDC hdc, const int * piAttribIList, const FLOAT * pfAttribFList, UINT nMaxFormats, int * piFormats, UINT * nNumFormats);
	extern BOOL (CODEGEN_FUNCPTR *GetPixelFormatAttribfvARB)(HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, const int * piAttributes, FLOAT * pfValues);
	extern BOOL (CODEGEN_FUNCPTR *GetPixelFormatAttribivARB)(HDC hdc, int iPixelFormat, int iLayerPlane, UINT nAttributes, const int * piAttributes, int * piValues);
	
	// Extension: ARB_create_context
	extern HGLRC (CODEGEN_FUNCPTR *CreateContextAttribsARB)(HDC hDC, HGLRC hShareContext, const int * attribList);
	
	// Extension: EXT_swap_control
	extern int (CODEGEN_FUNCPTR *GetSwapIntervalEXT)(void);
	extern BOOL (CODEGEN_FUNCPTR *SwapIntervalEXT)(int interval);
	
	// Extension: NV_swap_group
	extern BOOL (CODEGEN_FUNCPTR *BindSwapBarrierNV)(GLuint group, GLuint barrier);
	extern BOOL (CODEGEN_FUNCPTR *JoinSwapGroupNV)(HDC hDC, GLuint group);
	extern BOOL (CODEGEN_FUNCPTR *QueryFrameCountNV)(HDC hDC, GLuint * count);
	extern BOOL (CODEGEN_FUNCPTR *QueryMaxSwapGroupsNV)(HDC hDC, GLuint * maxGroups, GLuint * maxBarriers);
	extern BOOL (CODEGEN_FUNCPTR *QuerySwapGroupNV)(HDC hDC, GLuint * group, GLuint * barrier);
	extern BOOL (CODEGEN_FUNCPTR *ResetFrameCountNV)(HDC hDC);
	
	namespace sys
	{
		void CheckExtensions(HDC hdc);
	}
}
#endif //WINDOWSGL_NOLOAD_STYLE_HPP
