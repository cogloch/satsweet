#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <gl\gl_core.hpp>
#include <optional>
#include "nanovg.h"

#include <vector>
#include <algorithm>
#include <vis.h>


namespace glnvg
{
	enum UniformLoc
	{
		LOC_VIEWSIZE,
		LOC_TEX,
		LOC_FRAG,
		MAX_LOCS
	};

	enum UniformBindings
	{
		FRAG_BINDING = 0,
	};

	struct Shader
	{
		GLuint m_shader;
		GLint m_loc[MAX_LOCS];

		int Create(const std::string& name, const char* header, const char* opts, const char* vshader, const char* fshader);
		void Delete();
		void GetUniforms();

	private:
        bool Compile(const GLuint shader, const std::string& name);
	};

	struct Texture
	{
		int id;
		GLuint tex;
		int width, height;
		int type;
		int flags;
	};

	struct Blend
	{
		GLenum srcRGB, dstRGB;
		GLenum srcAlpha, dstAlpha;
	};

	bool operator==(const Blend&, const Blend&);

	struct Call
	{
		enum class Type
		{
			NONE = 0,
			FILL,
			CONVEXFILL,
			STROKE,
			TRIANGLES,
		} type;

		int image;
		int pathOffset;
		int pathCount;
		int triangleOffset;
		int triangleCount;
		int uniformOffset;
		Blend blendFunc;
	};

	struct Path
	{
		int fillOffset;
		int fillCount;
		int strokeOffset;
		int strokeCount;
	};

	enum class ShaderType
	{
		FILLGRAD,
		FILLIMG,
		SIMPLE,
		IMG
	};

	struct FragUniforms
	{
		float scissorMat[12]; // matrices are actually 3 vec4s
		float paintMat[12];
		struct nvg::color innerCol;
		struct nvg::color outerCol;
		float scissorExt[2];
		float scissorScale[2];
		float extent[2];
		float radius;
		float feather;
		float strokeMult;
		float strokeThr;
		int texType;
		ShaderType type;
	};

	struct Context
	{
		Shader shader;
		std::vector<Texture> textures;
		float view[2];
		int textureId;
		GLuint vertBuf;
		GLuint vertArr;
		GLuint fragBuf;
		int fragSize;
		int flags;

		// Per frame buffers
        std::vector<Call> m_calls;
        size_t m_numCalls = 0;
        std::vector<Path> m_paths;
        size_t m_numPaths = 0;
		nvg::vertex* m_verts;
		int m_cverts;
		int m_nverts;
		u8* uniforms;
		int cuniforms;
		int nuniforms;

		// Cached state
		GLuint m_boundTexture;
		GLuint m_stencilMask;
		GLenum m_stencilFunc;
		GLint  m_stencilFuncRef;
		GLuint m_stencilFuncMask;
		Blend  m_blendFunc;

		void BindTexture(GLuint tex);
		void StencilMask(GLuint mask);
		void StencilFunc(GLenum func, GLint ref, GLuint mask);
		void BlendFuncSeparate(const Blend& blend);
		Texture* AllocTexture();
		Texture* FindTexture(int id);
		int DeleteTexture(int id);
		void CheckError(const char* str);
		int ConvertPaint(FragUniforms* frag, const nvg::paint& paint, const nvg::scissor& scissor, float width, float fringe, float strokeThr);
		void SetUniforms(int uniformOffset, int image);
		Call& AllocCall();
		int AllocPaths(int n);
		int AllocVerts(int n);
		int AllocFragUniforms(int n);
		FragUniforms* FragUniformPtr(int i);

        int CreateTexture(int type, int w, int h, int imageFlags, const u8* data);
	    int UpdateTexture(int image, int x, int y, int w, int h, const u8* data);
	    std::optional<Extent> GetTextureSize(const int image);
	    
        ~Context();
	    void Create();
	    void Cancel();
	    void Flush();

		void Fill(const Call& call);
		void ConvexFill(const Call& call);
        void RenderFill(nvg::CompositeOperationState, const nvg::paint&, const nvg::scissor&, float fringe, const float* bounds, const nvg::Path* paths, int npaths);
        
		void Stroke(const Call& call);
	    void RenderStroke(nvg::CompositeOperationState, const nvg::paint&, const nvg::scissor&, float fringe, float strokeWidth, const nvg::Path* paths, int npaths);
        
		void Triangles(const Call& call);
	    void RenderTriangles(nvg::CompositeOperationState, const nvg::paint&, const nvg::scissor&, const nvg::vertex* verts, int nverts);
	    
	    void SetViewport(const int width, const int height);
	};

    int CreateImageFromHandle(nvg::Context*, const GLuint textureId, const int w, const int h, const int flags);
    GLuint GetImageHandle(nvg::Context*, const int image);
}
