#pragma once
#include <string>
#include <vector>
#include <optional>
#include <glm\glm.hpp>
#include <vis.h>
#include <array>
#include "fontstash.h"

#undef CreateFont
#undef WINDING

#define NVG_PI 3.14159265358979323846264338327f

#pragma warning(push)
#pragma warning(disable: 4201)  // nonstandard extension used : nameless struct/union

#define NVG_MAX_STATES 32
#define NVG_MAX_FONTIMAGES 4

namespace fons
{
	struct Context;
}

struct Rect
{
	Rect();
	Rect(float xmin, float ymin, float xmax, float ymax);
	void ScaleUniform(const float);

	union
	{
		float coord[4];
		struct
		{
			float xmin, ymin, xmax, ymax;
		};
	};
};

namespace glnvg
{
    struct Context;
}

namespace nvg
{
    enum CreateFlags
    {
        // Flag indicating if geometry based anti-aliasing is used (may not be needed when using MSAA).
        ANTIALIAS = 1 << 0,
        // Flag indicating if strokes should be drawn using stencil buffer. The rendering will be a little
        // slower, but path overlaps (i.e. self-intersecting or sharp turns) will be drawn just once.
        STENCIL_STROKES = 1 << 1,
        // Flag indicating that additional debug checks are done.
        DEBUG = 1 << 2,
    };

    // These are additional flags on top of NVGimageFlags.
    enum ImageFlagsGL
    {
        IMAGE_NODELETE = 1 << 16,	// Do not delete GL texture handle.
    };

	enum compositeOperation
	{
		NVG_SOURCE_OVER,
		NVG_SOURCE_IN,
		NVG_SOURCE_OUT,
		NVG_ATOP,
		NVG_DESTINATION_OVER,
		NVG_DESTINATION_IN,
		NVG_DESTINATION_OUT,
		NVG_DESTINATION_ATOP,
		NVG_LIGHTER,
		NVG_COPY,
		NVG_XOR,
	};

	struct CompositeOperationState
	{
		int srcRGB;
		int dstRGB;
		int srcAlpha;
		int dstAlpha;
	};

	enum Commands
	{
		MOVETO   = 0,
		LINETO   = 1,
		BEZIERTO = 2,
		CLOSE    = 3,
		WINDING  = 4
	};

	enum pointFlags
	{
		NVG_PT_CORNER = 0x01,
		NVG_PT_LEFT = 0x02,
		NVG_PT_BEVEL = 0x04,
		NVG_PR_INNERBEVEL = 0x08,
	};

	struct color
	{
		color() = default;

		color(float c[])
		{
			for (int i = 0; i < 4; i++)
				rgba[i] = c[i];
		}

		color(float r_, float g_, float b_, float a_)
		{
			r = r_;
			g = g_;
			b = b_;
			a = a_;
		}

		union
		{
			float rgba[4];
			struct
			{
				float r, g, b, a;
			};
		};
	};

	struct paint
	{
		paint();

		float xform[6];
		float extent[2];
		float radius;
		float feather;
		color innerColor;
		color outerColor;
		int image;

		void SetColor(const color&);
	};

	struct scissor
	{
		float xform[6];
		float extent[2];
	};
	
	struct State
	{
		void Reset();
		float GetFontScale();
		bool IsFontValid() const;

		CompositeOperationState compositeOperation;
		int shapeAntiAlias;
		paint fill;
		paint stroke;
		float strokeWidth;
		float miterLimit;
		int lineJoin;
		int lineCap;
		float alpha;
		float xform[6];
		scissor scissor;
		float fontSize;
		float letterSpacing;
		float lineHeight;
		float fontBlur;
		int textAlign;
		int fontId;
	};

	struct point
	{
		float x, y;
		float dx, dy;
		float len;
		float dmx, dmy;
		unsigned char flags;
	};

	enum texture
	{
		NVG_TEXTURE_ALPHA = 0x01,
		NVG_TEXTURE_RGBA = 0x02,
	};

	struct vertex
	{
		float x, y, u, v;
	};

	struct Path
	{
		Path(const int first, const int winding);

		int first;
		int count;
		u8 closed;
		int nbevel;
		vertex* fill;
		int nfill;
		vertex* stroke;
		int nstroke;
		int winding;
		int convex;
	};

	struct PathCache
	{
		PathCache();
		void Clear();
		void AddPath(const int first, const int winding);

		std::vector<Path> paths;
		std::vector<vertex> verts;
		std::vector<point> points;
		float bounds[4];
	};

	enum winding 
	{
		NVG_CCW = 1,			// Winding for solid shapes
		NVG_CW = 2,				// Winding for holes
	};

	enum solidity 
	{
		NVG_SOLID = 1,			// CCW
		NVG_HOLE = 2,			// CW
	};

	enum lineCap 
	{
		NVG_BUTT,
		NVG_ROUND,
		NVG_SQUARE,
		NVG_BEVEL,
		NVG_MITER,
	};

	enum Align 
	{
		// Horizontal align
		LEFT 	 = 1<<0, // Default, align text horizontally to left.
		CENTER 	 = 1<<1, // Align text horizontally to center.
		RIGHT 	 = 1<<2, // Align text horizontally to right.
		// Vertical align
		TOP 	 = 1<<3, // Align text vertically to top.
		MIDDLE	 = 1<<4, // Align text vertically to middle.
		BOTTOM	 = 1<<5, // Align text vertically to bottom.
		BASELINE = 1<<6, // Default, align text vertically to baseline.
	};

	enum blendFactor 
	{
		NVG_ZERO = 1<<0,
		NVG_ONE = 1<<1,
		NVG_SRC_COLOR = 1<<2,
		NVG_ONE_MINUS_SRC_COLOR = 1<<3,
		NVG_DST_COLOR = 1<<4,
		NVG_ONE_MINUS_DST_COLOR = 1<<5,
		NVG_SRC_ALPHA = 1<<6,
		NVG_ONE_MINUS_SRC_ALPHA = 1<<7,
		NVG_DST_ALPHA = 1<<8,
		NVG_ONE_MINUS_DST_ALPHA = 1<<9,
		NVG_SRC_ALPHA_SATURATE = 1<<10,
	};

	struct glyphPosition 
	{
		const char* str;	// Position of the glyph in the input string.
		float x;			// The x-coordinate of the logical glyph position.
		float minx, maxx;	// The bounds of the glyph shape.
	};

	struct textRow 
	{
		const char* start;	// Pointer to the input text where the row starts.
		const char* end;	// Pointer to the input text where the row ends (one past the last character).
		const char* next;	// Pointer to the beginning of the next row.
		float width;		// Logical width of the row.
		float minx, maxx;	// Actual bounds of the row. Logical with and bounds can differ because of kerning and some parts over extending.
	};

	enum imageFlags 
	{
		NVG_IMAGE_GENERATE_MIPMAPS	= 1<<0,     // Generate mipmaps during creation of the image.
		NVG_IMAGE_REPEATX			= 1<<1,		// Repeat image in X direction.
		NVG_IMAGE_REPEATY			= 1<<2,		// Repeat image in Y direction.
		NVG_IMAGE_FLIPY				= 1<<3,		// Flips (inverses) image in Y direction when rendered.
		NVG_IMAGE_PREMULTIPLIED		= 1<<4,		// Image data has premultiplied alpha.
		NVG_IMAGE_NEAREST			= 1<<5,		// Image interpolation is Nearest instead Linear
	};

	struct Context
	{
        glnvg::Context* glContext;
        int edgeAA;
		std::vector<float> commands;
		float commandx, commandy;
		State states[NVG_MAX_STATES];
		int nstates;
		PathCache* cache;
		float tessTol;
		float distTol;
		float fringeWidth;
		float devicePxRatio;
		fons::Context* fs;
		std::array<int, NVG_MAX_FONTIMAGES> fontImages;
		int fontImageIdx;
		int drawCallCount;
		int fillTriCount;
		int strokeTriCount;
		int textTriCount;

		// Constructor and destructor, called by the render back-end.
		Context(const int flags);
		~Context();

		void SetDevicePixelRatio(const float ratio);
		void FlushTextTexture();
		Path* GetLastPath();
		void AddPath();
		point* GetLastPoint();
		vertex* AllocTempVerts(const int nverts);

		void RenderText(vertex* verts, int nverts);
		bool AllocTextAtlas();
		void FlattenPaths();

		void TesselateBezier(float x1, float y1, float x2, float y2,
							 float x3, float y3, float x4, float y4,
							 int level, int type);

		// Begin drawing a new frame
		// Calls to nanovg drawing API should be wrapped in nvgBeginFrame() & nvgEndFrame()
		// nvgBeginFrame() defines the size of the window to render to in relation currently
		// set viewport (i.e. glViewport on GL backends). Device pixel ration allows to
		// control the rendering on Hi-DPI devices.
		// For example, GLFW returns two dimension for an opened window: window size and
		// frame buffer size. In that case you would set windowWidth/Height to the window size
		// devicePixelRatio to: frameBufferWidth / windowWidth.
		void BeginFrame(int windowWidth, int windowHeight, float devicePixelRatio);

		// Cancels drawing the current frame.
		void CancelFrame();

		// Ends drawing flushing remaining render state.
		void EndFrame();

		//
		// State Handling
		//
		// NanoVG contains state which represents how paths will be rendered.
		// The state contains transform, fill and stroke styles, text and font styles,
		// and scissor clipping.

		// Pushes and saves the current render state into a state stack.
		// A matching nvgRestore() must be used to restore the state.
		void Save();

		// Pops and restores current render state.
		void Restore();

		// Resets current render state to default values. Does not affect the render state stack.
		void Reset();

		State* GetState();

		//
		// Render styles
		//
		// Fill and stroke render style can be either a solid color or a paint which is a gradient or a pattern.
		// Solid color is simply defined as a color value, different kinds of paints can be created
		// using nvgLinearGradient(), nvgBoxGradient(), nvgRadialGradient() and nvgImagePattern().
		//
		// Current render style can be saved and restored using Save() and Restore().

		// Sets whether to draw antialias for Stroke() and Fill(). It's enabled by default.
		void ShapeAntiAlias(int enabled);

		// Sets current stroke style to a solid color.
		void StrokeColor(color color);

		// Sets current stroke style to a paint, which can be a one of the gradients or a pattern.
		void StrokePaint(paint paint);

		// Sets current fill style to a solid color.
		void FillColor(color color);

		// Sets current fill style to a paint, which can be a one of the gradients or a pattern.
		void FillPaint(paint paint);

		// Sets the miter limit of the stroke style.
		// Miter limit controls when a sharp corner is beveled.
		void MiterLimit(float limit);

		// Sets the stroke width of the stroke style.
		void StrokeWidth(float size);

		// Sets how the end of the line (cap) is drawn,
		// Can be one of: NVG_BUTT (default), NVG_ROUND, NVG_SQUARE.
		void LineCap(int cap);

		// Sets how sharp path corners are drawn.
		// Can be one of NVG_MITER (default), NVG_ROUND, NVG_BEVEL.
		void LineJoin(int join);

		// Sets the transparency applied to all rendered shapes.
		// Already transparent paths will get proportionally more transparent as well.
		void GlobalAlpha(float alpha);

		//
		// Paths
		//
		// Drawing a new shape starts with BeginPath(), it clears all the currently defined paths.
		// Then you define one or more paths and sub-paths which describe the shape. The are functions
		// to draw common shapes like rectangles and circles, and lower level step-by-step functions,
		// which allow to define a path curve by curve.
		//
		// NanoVG uses even-odd fill rule to draw the shapes. Solid shapes should have counter clockwise
		// winding and holes should have counter clockwise order. To specify winding of a path you can
		// call PathWinding(). This is useful especially for the common shapes, which are drawn CCW.
		//
		// Finally you can fill the path using current fill style by calling Fill(), and stroke it
		// with current stroke style by calling Stroke().
		//
		// The curve segments and sub-paths are transformed by the current transform.

		// Clears the current path and sub-paths.
		void BeginPath();

		// Starts new sub-path with specified point as first point.
		void MoveTo(float x, float y);

		// Adds line segment from the last point in the path to the specified point.
		void LineTo(float x, float y);

		// Adds cubic bezier segment from last point in the path via two control points to the specified point.
		void BezierTo(float c1x, float c1y, float c2x, float c2y, float x, float y);

		// Adds quadratic bezier segment from last point in the path via a control point to the specified point.
		void QuadTo(float cx, float cy, float x, float y);

		// Adds an arc segment at the corner defined by the last path point, and two specified points.
		void ArcTo(float x1, float y1, float x2, float y2, float radius);

		// Closes current sub-path with a line segment.
		void ClosePath();

		// Sets the current sub-path winding, see NVGwinding and NVGsolidity.
		void PathWinding(int dir);

		// Creates new circle arc shaped sub-path. The arc center is at cx,cy, the arc radius is r,
		// and the arc is drawn from angle a0 to a1, and swept in direction dir (NVG_CCW, or NVG_CW).
		// Angles are specified in radians.
		void Arc(float cx, float cy, float r, float a0, float a1, int dir);

		// Creates new rectangle shaped sub-path.
		void CreateRect(float x, float y, float w, float h);

		// Creates new rounded rectangle shaped sub-path.
		void RoundedRect(float x, float y, float w, float h, float r);

		// Creates new rounded rectangle shaped sub-path with varying radii for each corner.
		void RoundedRectVarying(float x, float y, float w, float h, float radTopLeft, float radTopRight, float radBottomRight, float radBottomLeft);

		// Creates new ellipse shaped sub-path.
		void Ellipse(float cx, float cy, float rx, float ry);

		// Creates new circle shaped sub-path.
		void Circle(float cx, float cy, float r);

		// Fills the current path with current fill style.
		void Fill();

		// Fills the current path with current stroke style.
		void Stroke();

		// Sets the font size of current text style.
		void SetFontSize(const float size);

		// Sets the blur of current text style.
		void FontBlur(float blur);

		// Sets the letter spacing of current text style.
		void TextLetterSpacing(float spacing);

		// Sets the proportional line height of current text style. The line height is specified as multiple of font size.
		void TextLineHeight(float lineHeight);

		// Sets the text align of current text style, see NVGalign for options.
		void TextAlign(int align);

		void SetFontFace(int font);
		void SetFontFace(const std::string& font);

		// Draws text string at specified location. If end is specified only the sub-string up to the end is drawn.
		float Text(float x, float y, const char* string, const char* end);

		// Draws multi-line text string at specified location wrapped at the specified width. If end is specified only the sub-string up to the end is drawn.
		// White space is stripped at the beginning of the rows, the text is split at word boundaries or when new-line characters are encountered.
		// Words longer than the max width are slit at nearest character (i.e. no hyphenation).
		void TextBox(float x, float y, float breakRowWidth, const char* string, const char* end);

		// Measures the specified text string. Parameter bounds should be a pointer to float[4],
		// if the bounding box of the text should be returned. The bounds value are [xmin,ymin, xmax,ymax]
		// Returns the horizontal advance of the measured text (i.e. where the next character should drawn).
		// Measured values are returned in local coordinate space.
		struct TextBoundsResult
		{
			Rect bounds;
			float advanceX;
		};
		std::optional<TextBoundsResult> TextBounds(float x, float y, const char* string, const char* end);
		
		// Measures the specified multi-text string. Parameter bounds should be a pointer to float[4], if the bounding box of the text should be returned. The bounds value are [xmin,ymin, xmax,ymax]
		// Measured values are returned in local coordinate space.
		std::optional<Rect> TextBoxBounds(float x, float y, float breakRowWidth, const char* string, const char* end);

		// Calculates the glyph x positions of the specified text. If end is specified only the sub-string will be used.
		// Measured values are returned in local coordinate space.
		std::optional<std::vector<glyphPosition>> TextGlyphPositions(float x, float y, const char* string, const char* end, int maxPositions);

		// Returns the vertical metrics based on the current text style.
		// Measured values are returned in local coordinate space.
		fons::TextMetrics ComputeTextMetrics();

		// Breaks the specified text into lines. If end is specified only the sub-string will be used.
		// White space is stripped at the beginning of the rows, the text is split at word boundaries or when new-line characters are encountered.
		// Words longer than the max width are slit at nearest character (i.e. no hyphenation).
		int TextBreakLines(const char* string, const char* end, float breakRowWidth, textRow* rows, int maxRows);

		// Debug function to dump cached path data.
		void DebugDumpPathCache();

		//
		// Composite operation
		//
		// The composite operations in NanoVG are modeled after HTML Canvas API, and
		// the blend func is based on OpenGL (see corresponding manuals for more info).
		// The colors in the blending state have premultiplied alpha.

		// Sets the composite operation. The op parameter should be one of NVGcompositeOperation.
		void GlobalCompositeOperation(int op);

		// Sets the composite operation with custom pixel arithmetic. The parameters should be one of NVGblendFactor.
		void GlobalCompositeBlendFunc(int sfactor, int dfactor);

		// Sets the composite operation with custom pixel arithmetic for RGB and alpha components separately. The parameters should be one of NVGblendFactor.
		void GlobalCompositeBlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha);

		//
		// Transforms
		//
		// The paths, gradients, patterns and scissor region are transformed by an transformation
		// matrix at the time when they are passed to the API.
		// The current transformation matrix is a affine matrix:
		//   [sx kx tx]
		//   [ky sy ty]
		//   [ 0  0  1]
		// Where: sx,sy define scaling, kx,ky skewing, and tx,ty translation.
		// The last row is assumed to be 0,0,1 and is not stored.
		//
		// Apart from ResetTransform(), each transformation function first creates
		// specific transformation matrix and pre-multiplies the current transformation by it.
		//
		// Current coordinate system (transformation) can be saved and restored using Save() and Restore().

		// Resets current transform to a identity matrix.
		void ResetTransform();

		// Premultiplies current coordinate system by specified matrix.
		// The parameters are interpreted as matrix as follows:
		//   [a c e]
		//   [b d f]
		//   [0 0 1]
		void Transform(float a, float b, float c, float d, float e, float f);

		// Translates current coordinate system.
		void Translate(float x, float y);

		// Rotates current coordinate system. Angle is specified in radians.
		void Rotate(float angle);

		// Skews the current coordinate system along X axis. Angle is specified in radians.
		void SkewX(float angle);

		// Skews the current coordinate system along Y axis. Angle is specified in radians.
		void SkewY(float angle);

		// Scales the current coordinate system.
		void Scale(float x, float y);

		// Stores the top part (a-f) of the current transformation matrix in to the specified buffer.
		//   [a c e]
		//   [b d f]
		//   [0 0 1]
		// There should be space for 6 floats in the return buffer for the values a-f.
		void CurrentTransform(float* xform);

		//
		// Images
		//
		// NanoVG allows you to load jpg, png, psd, tga, pic and gif files to be used for rendering.
		// In addition you can upload your own image. The image loading is provided by stb_image.
		// The parameter imageFlags is combination of flags defined in NVGimageFlags.

		// Creates image by loading it from the disk from specified file name.
		// Returns handle to the image.
		int CreateImage(const char* filename, int imageFlags);

		// Creates image by loading it from the specified chunk of memory.
		// Returns handle to the image.
		int CreateImageMem(int imageFlags, unsigned char* data, int ndata);

		// Creates image from specified image data.
		// Returns handle to the image.
		int CreateImageRGBA(int w, int h, int imageFlags, const unsigned char* data);

		// Updates image data specified by image handle.
		void UpdateImage(int image, const unsigned char* data);

		// Returns the dimensions of a created image.
		void ImageSize(int image, int* w, int* h);

		// Deletes created image.
		void DeleteImage(int image);

		//
		// Paints
		//
		// NanoVG supports four types of paints: linear gradient, box gradient, radial gradient and image pattern.
		// These can be used as paints for strokes and fills.

		// Creates and returns a linear gradient. Parameters (sx,sy)-(ex,ey) specify the start and end coordinates
		// of the linear gradient, icol specifies the start color and ocol the end color.
		// The gradient is transformed by the current transform when it is passed to FillPaint() or StrokePaint().
		paint LinearGradient(float sx, float sy, float ex, float ey, color icol, color ocol);

		// Creates and returns a box gradient. Box gradient is a feathered rounded rectangle, it is useful for rendering
		// drop shadows or highlights for boxes. Parameters (x,y) define the top-left corner of the rectangle,
		// (w,h) define the size of the rectangle, r defines the corner radius, and f feather. Feather defines how blurry
		// the border of the rectangle is. Parameter icol specifies the inner color and ocol the outer color of the gradient.
		// The gradient is transformed by the current transform when it is passed to FillPaint() or StrokePaint().
		paint BoxGradient(float x, float y, float w, float h, float r, float f, color icol, color ocol);

		// Creates and returns a radial gradient. Parameters (cx,cy) specify the center, inr and outr specify
		// the inner and outer radius of the gradient, icol specifies the start color and ocol the end color.
		// The gradient is transformed by the current transform when it is passed to FillPaint() or StrokePaint().
		paint RadialGradient(float cx, float cy, float inr, float outr, color icol, color ocol);

		// Creates and returns an image patter. Parameters (ox,oy) specify the left-top location of the image pattern,
		// (ex,ey) the size of one image, angle rotation around the top-left corner, image is handle to the image to render.
		// The gradient is transformed by the current transform when it is passed to FillPaint() or StrokePaint().
		paint ImagePattern(float ox, float oy, float ex, float ey, float angle, int image, float alpha);

		//
		// Scissoring
		//
		// Scissoring allows you to clip the rendering into a rectangle. This is useful for various
		// user interface cases like rendering a text edit or a timeline.

		// Sets the current scissor rectangle.
		// The scissor rectangle is transformed by the current transform.
		void Scissor(float x, float y, float w, float h);

		// Intersects current scissor rectangle with the specified rectangle.
		// The scissor rectangle is transformed by the current transform.
		// Note: in case the rotation of previous scissor rect differs from
		// the current one, the intersection will be done between the specified
		// rectangle and the previous scissor rectangle transformed in the current
		// transform space. The resulting shape is always rectangle.
		void IntersectScissor(float x, float y, float w, float h);

		// Reset and disables scissoring.
		void ResetScissor();

		void AppendCommands(float* vals, int nvals);
	};

	//
	// Color utils
	//
	// Colors in NanoVG are stored as unsigned ints in ABGR format.

	// Returns a color value from red, green, blue values. Alpha will be set to 255 (1.0f).
	color nvgRGB(unsigned char r, unsigned char g, unsigned char b);

	// Returns a color value from red, green, blue values. Alpha will be set to 1.0f.
	color RGBf(float r, float g, float b);


	// Returns a color value from red, green, blue and alpha values.
	color RGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a);

	// Returns a color value from red, green, blue and alpha values.
	color RGBAf(float r, float g, float b, float a);


	// Linearly interpolates from color c0 to c1, and returns resulting color value.
	color LerpRGBA(color c0, color c1, float u);

	// Sets transparency of a color value.
	color TransRGBA(color c0, unsigned char a);

	// Sets transparency of a color value.
	color TransRGBAf(color c0, float a);

	// Returns color value specified by hue, saturation and lightness.
	// HSL values are all in range [0..1], alpha will be set to 255.
	color HSL(float h, float s, float l);

	// Returns color value specified by hue, saturation and lightness and alpha.
	// HSL values are all in range [0..1], alpha in range [0..255]
	color HSLA(float h, float s, float l, unsigned char a);

	// The following functions can be used to make calculations on 2x3 transformation matrices.
	// A 2x3 matrix is represented as float[6].

	// Sets the transform to identity matrix.
	void TransformIdentity(float* dst);

	// Sets the transform to translation matrix matrix.
	void TransformTranslate(float* dst, float tx, float ty);

	// Sets the transform to scale matrix.
	void TransformScale(float* dst, float sx, float sy);

	// Sets the transform to rotate matrix. Angle is specified in radians.
	void TransformRotate(float* dst, float a);

	// Sets the transform to skew-x matrix. Angle is specified in radians.
	void TransformSkewX(float* dst, float a);

	// Sets the transform to skew-y matrix. Angle is specified in radians.
	void TransformSkewY(float* dst, float a);

	// Sets the transform to the result of multiplication of two transforms, of A = A*B.
	void TransformMultiply(float* dst, const float* src);

	// Sets the transform to the result of multiplication of two transforms, of A = B*A.
	void TransformPremultiply(float* dst, const float* src);

	// Sets the destination to inverse of specified transform.
	// Returns 1 if the inverse could be calculated, else 0.
	int TransformInverse(float* dst, const float* src);

	// Transform a point by given transform.
	void TransformPoint(float* dstx, float* dsty, const float* xform, float srcx, float srcy);

	// Converts degrees to radians and vice versa.
	float DegToRad(float deg);
	float RadToDeg(float rad);
}

#pragma warning(pop)

#define NVG_NOTUSED(v) for (;;) { (void)(1 ? (void)0 : ( (void)(v) ) ); break; }
