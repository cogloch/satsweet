#include "stdafx.h"
#include "nanovg_gl.h"


bool glnvg::operator==(const Blend& lhs, const Blend & rhs)
{
	return (lhs.srcRGB == rhs.srcRGB) &&
		   (lhs.dstRGB == rhs.dstRGB) &&
		   (lhs.srcAlpha == rhs.srcAlpha) &&
		   (lhs.dstAlpha == rhs.dstAlpha);
}

int maxi(const int a, const int b)
{
	return a > b ? a : b;
}

void glnvg::Context::BindTexture(GLuint tex)
{
	if (m_boundTexture == tex) return;

	m_boundTexture = tex;
	gl::BindTexture(gl::TEXTURE_2D, tex);
}

void glnvg::Context::StencilMask(GLuint mask)
{
	if (m_stencilMask == mask) return;

	m_stencilMask = mask;
	gl::StencilMask(mask);
}

void glnvg::Context::StencilFunc(GLenum func, GLint ref, GLuint mask)
{
	if (m_stencilFunc == func && m_stencilFuncRef == ref && m_stencilFuncMask == mask) return;

	m_stencilFunc = func;
	m_stencilFuncRef = ref;
	m_stencilFuncMask = mask;
	gl::StencilFunc(func, ref, mask);
}

void glnvg::Context::BlendFuncSeparate(const Blend& blend)
{
	if (m_blendFunc == blend) return;

	m_blendFunc = blend;
	gl::BlendFuncSeparate(blend.srcRGB, blend.dstRGB, blend.srcAlpha, blend.dstAlpha);
}

glnvg::Texture* glnvg::Context::AllocTexture()
{
	auto tex = FindTexture(0);

	if (!tex) 
	{
		textures.push_back(Texture());
		tex = &textures[textures.size() - 1];
	}

	memset(tex, 0, sizeof(*tex));
	tex->id = ++textureId;

	return tex;
}

glnvg::Texture* glnvg::Context::FindTexture(int id)
{
	for (auto& texture : textures)
		if (texture.id == id)
			return &texture;
	return nullptr;
}

int glnvg::Context::DeleteTexture(int id)
{
	auto texture = FindTexture(id);
	if (!texture) return 0;

	if (texture->tex && (texture->flags & nvg::IMAGE_NODELETE) == 0)
		gl::DeleteTextures(1, &texture->tex);
	memset(texture, 0, sizeof(*texture));
	return 1;
}

void glnvg::Context::CheckError(const char* str)
{
	if ((flags & nvg::DEBUG) == 0) return;
	
	const GLenum err = gl::GetError();
	if (err != gl::NO_ERROR_) 
		printf("Error %08x after %s\n", err, str);
}

bool glnvg::Shader::Compile(const GLuint shader, const std::string& name)
{
    gl::CompileShader(shader);
    GLint status;
    gl::GetShaderiv(shader, gl::COMPILE_STATUS, &status);
    if (status == gl::TRUE_)
        return true;

    GLchar str[512 + 1];
    GLsizei len = 0;
    gl::GetShaderInfoLog(shader, 512, &len, str);
    if (len > 512) len = 512;
    str[len] = '\0';
    std::cout << "Shader " << name << " error: " << str << "\n";
    return false;
}

int glnvg::Shader::Create(const std::string& name, const char* header, const char* opts, const char* vshader, const char* fshader)
{
	const char* str[3];
	str[0] = header;
	str[1] = opts ? opts : "";

	memset(this, 0, sizeof(*this));

	m_shader = gl::CreateProgram();
	GLuint vert = gl::CreateShader(gl::VERTEX_SHADER);
	GLuint frag = gl::CreateShader(gl::FRAGMENT_SHADER);
	str[2] = vshader;
	gl::ShaderSource(vert, 3, str, 0);
	str[2] = fshader;
	gl::ShaderSource(frag, 3, str, 0);

    if (!(Compile(vert, name + "/vert") && Compile(frag, name + "/frag")))
        return 0;
	
	gl::AttachShader(m_shader, vert);
	gl::AttachShader(m_shader, frag);

	GLint status;
	gl::BindAttribLocation(m_shader, 0, "vertex");
	gl::BindAttribLocation(m_shader, 1, "tcoord");

	gl::LinkProgram(m_shader);

    gl::DeleteShader(vert);
    gl::DeleteShader(frag);

    gl::GetProgramiv(m_shader, gl::LINK_STATUS, &status);
	if (status != gl::TRUE_)
	{
        GLchar debug[512 + 1];
        GLsizei len = 0;
        gl::GetProgramInfoLog(m_shader, 512, &len, debug);
        if (len > 512) len = 512;
        debug[len] = '\0';
        std::cout << "Program " << name << " error: " << debug << '\n';
		return 0;
	}

	return 1;
}

void glnvg::Shader::Delete()
{
	gl::DeleteProgram(m_shader);
}

void glnvg::Shader::GetUniforms()
{
	m_loc[LOC_VIEWSIZE] = gl::GetUniformLocation(m_shader, "viewSize");
	m_loc[LOC_TEX] = gl::GetUniformLocation(m_shader, "tex");
	m_loc[LOC_FRAG] = gl::GetUniformBlockIndex(m_shader, "frag");
}

void glnvg::Context::Create()
{
	int align = 4;

	const char* shaderHeader =
		"#version 150 core\n"
		"#define NANOVG_GL3 1\n"

		"#define USE_UNIFORMBUFFER 1\n"
		"\n";

	const char* fillVertShader =
		"#ifdef NANOVG_GL3\n"
		"	uniform vec2 viewSize;\n"
		"	in vec2 vertex;\n"
		"	in vec2 tcoord;\n"
		"	out vec2 ftcoord;\n"
		"	out vec2 fpos;\n"
		"#else\n"
		"	uniform vec2 viewSize;\n"
		"	attribute vec2 vertex;\n"
		"	attribute vec2 tcoord;\n"
		"	varying vec2 ftcoord;\n"
		"	varying vec2 fpos;\n"
		"#endif\n"
		"void main(void) {\n"
		"	ftcoord = tcoord;\n"
		"	fpos = vertex;\n"
		"	gl_Position = vec4(2.0*vertex.x/viewSize.x - 1.0, 1.0 - 2.0*vertex.y/viewSize.y, 0, 1);\n"
		"}\n";

	const char* fillFragShader =
		"#ifdef GL_ES\n"
		"#if defined(GL_FRAGMENT_PRECISION_HIGH) || defined(NANOVG_GL3)\n"
		" precision highp float;\n"
		"#else\n"
		" precision mediump float;\n"
		"#endif\n"
		"#endif\n"
		"#ifdef NANOVG_GL3\n"
		"#ifdef USE_UNIFORMBUFFER\n"
		"	layout(std140) uniform frag {\n"
		"		mat3 scissorMat;\n"
		"		mat3 paintMat;\n"
		"		vec4 innerCol;\n"
		"		vec4 outerCol;\n"
		"		vec2 scissorExt;\n"
		"		vec2 scissorScale;\n"
		"		vec2 extent;\n"
		"		float radius;\n"
		"		float feather;\n"
		"		float strokeMult;\n"
		"		float strokeThr;\n"
		"		int texType;\n"
		"		int type;\n"
		"	};\n"
		"#else\n" // NANOVG_GL3 && !USE_UNIFORMBUFFER
		"	uniform vec4 frag[UNIFORMARRAY_SIZE];\n"
		"#endif\n"
		"	uniform sampler2D tex;\n"
		"	in vec2 ftcoord;\n"
		"	in vec2 fpos;\n"
		"	out vec4 outColor;\n"
		"#else\n" // !NANOVG_GL3
		"	uniform vec4 frag[UNIFORMARRAY_SIZE];\n"
		"	uniform sampler2D tex;\n"
		"	varying vec2 ftcoord;\n"
		"	varying vec2 fpos;\n"
		"#endif\n"
		"#ifndef USE_UNIFORMBUFFER\n"
		"	#define scissorMat mat3(frag[0].xyz, frag[1].xyz, frag[2].xyz)\n"
		"	#define paintMat mat3(frag[3].xyz, frag[4].xyz, frag[5].xyz)\n"
		"	#define innerCol frag[6]\n"
		"	#define outerCol frag[7]\n"
		"	#define scissorExt frag[8].xy\n"
		"	#define scissorScale frag[8].zw\n"
		"	#define extent frag[9].xy\n"
		"	#define radius frag[9].z\n"
		"	#define feather frag[9].w\n"
		"	#define strokeMult frag[10].x\n"
		"	#define strokeThr frag[10].y\n"
		"	#define texType int(frag[10].z)\n"
		"	#define type int(frag[10].w)\n"
		"#endif\n"
		"\n"
		"float sdroundrect(vec2 pt, vec2 ext, float rad) {\n"
		"	vec2 ext2 = ext - vec2(rad,rad);\n"
		"	vec2 d = abs(pt) - ext2;\n"
		"	return min(max(d.x,d.y),0.0) + length(max(d,0.0)) - rad;\n"
		"}\n"
		"\n"
		"// Scissoring\n"
		"float scissorMask(vec2 p) {\n"
		"	vec2 sc = (abs((scissorMat * vec3(p,1.0)).xy) - scissorExt);\n"
		"	sc = vec2(0.5,0.5) - sc * scissorScale;\n"
		"	return clamp(sc.x,0.0,1.0) * clamp(sc.y,0.0,1.0);\n"
		"}\n"
		"#ifdef EDGE_AA\n"
		"// Stroke - from [0..1] to clipped pyramid, where the slope is 1px.\n"
		"float strokeMask() {\n"
		"	return min(1.0, (1.0-abs(ftcoord.x*2.0-1.0))*strokeMult) * min(1.0, ftcoord.y);\n"
		"}\n"
		"#endif\n"
		"\n"
		"void main(void) {\n"
		"   vec4 result;\n"
		"	float scissor = scissorMask(fpos);\n"
		"#ifdef EDGE_AA\n"
		"	float strokeAlpha = strokeMask();\n"
		"	if (strokeAlpha < strokeThr) discard;\n"
		"#else\n"
		"	float strokeAlpha = 1.0;\n"
		"#endif\n"
		"	if (type == 0) {			// Gradient\n"
		"		// Calculate gradient color using box gradient\n"
		"		vec2 pt = (paintMat * vec3(fpos,1.0)).xy;\n"
		"		float d = clamp((sdroundrect(pt, extent, radius) + feather*0.5) / feather, 0.0, 1.0);\n"
		"		vec4 color = mix(innerCol,outerCol,d);\n"
		"		// Combine alpha\n"
		"		color *= strokeAlpha * scissor;\n"
		"		result = color;\n"
		"	} else if (type == 1) {		// Image\n"
		"		// Calculate color fron texture\n"
		"		vec2 pt = (paintMat * vec3(fpos,1.0)).xy / extent;\n"
		"#ifdef NANOVG_GL3\n"
		"		vec4 color = texture(tex, pt);\n"
		"#else\n"
		"		vec4 color = texture2D(tex, pt);\n"
		"#endif\n"
		"		if (texType == 1) color = vec4(color.xyz*color.w,color.w);"
		"		if (texType == 2) color = vec4(color.x);"
		"		// Apply color tint and alpha.\n"
		"		color *= innerCol;\n"
		"		// Combine alpha\n"
		"		color *= strokeAlpha * scissor;\n"
		"		result = color;\n"
		"	} else if (type == 2) {		// Stencil fill\n"
		"		result = vec4(1,1,1,1);\n"
		"	} else if (type == 3) {		// Textured tris\n"
		"#ifdef NANOVG_GL3\n"
		"		vec4 color = texture(tex, ftcoord);\n"
		"#else\n"
		"		vec4 color = texture2D(tex, ftcoord);\n"
		"#endif\n"
		"		if (texType == 1) color = vec4(color.xyz*color.w,color.w);"
		"		if (texType == 2) color = vec4(color.x);"
		"		color *= scissor;\n"
		"		result = color * innerCol;\n"
		"	}\n"
		"#ifdef NANOVG_GL3\n"
		"	outColor = result;\n"
		"#else\n"
		"	gl_FragColor = result;\n"
		"#endif\n"
		"}\n";

	CheckError("init");

	if (flags & nvg::ANTIALIAS) {
		if (shader.Create("shader", shaderHeader, "#define EDGE_AA 1\n", fillVertShader, fillFragShader) == 0)
			throw "dicks";
	}
	else {
		if (shader.Create("shader", shaderHeader, NULL, fillVertShader, fillFragShader) == 0)
			throw "dicks";
	}

	CheckError("uniform locations");
	shader.GetUniforms();
	
	// Create dynamic vertex array
	gl::GenVertexArrays(1, &vertArr);
	gl::GenBuffers(1, &vertBuf);

	// Create UBOs
	gl::UniformBlockBinding(shader.m_shader, shader.m_loc[LOC_FRAG], FRAG_BINDING);
	gl::GenBuffers(1, &fragBuf);
	gl::GetIntegerv(gl::UNIFORM_BUFFER_OFFSET_ALIGNMENT, &align);
	fragSize = sizeof(FragUniforms) + align - sizeof(FragUniforms) % align;

	CheckError("create done");
	
	gl::Finish();
}

int glnvg::Context::CreateTexture(int type, int w, int h, int imageFlags, const u8* data)
{
    const auto tex = AllocTexture();

    if (!tex) return 0;

    gl::GenTextures(1, &tex->tex);
    tex->width = w;
    tex->height = h;
    tex->type = type;
    tex->flags = imageFlags;
    BindTexture(tex->tex);

    gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
    gl::PixelStorei(gl::UNPACK_ROW_LENGTH, tex->width);
    gl::PixelStorei(gl::UNPACK_SKIP_PIXELS, 0);
    gl::PixelStorei(gl::UNPACK_SKIP_ROWS, 0);

    if (type == nvg::NVG_TEXTURE_RGBA)
        gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA, w, h, 0, gl::RGBA, gl::UNSIGNED_BYTE, data);
    else
        gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RED, w, h, 0, gl::RED, gl::UNSIGNED_BYTE, data);

    if (imageFlags & nvg::NVG_IMAGE_GENERATE_MIPMAPS)
    {
        if (imageFlags & nvg::NVG_IMAGE_NEAREST)
        {
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST_MIPMAP_NEAREST);
        }
        else
        {
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_LINEAR);
        }
    }
    else
    {
        if (imageFlags & nvg::NVG_IMAGE_NEAREST)
        {
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST);
        }
        else
        {
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR);
        }
    }

    if (imageFlags & nvg::NVG_IMAGE_NEAREST)
    {
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST);
    }
    else
    {
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
    }

    if (imageFlags & nvg::NVG_IMAGE_REPEATX)
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT);
    else
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE);

    if (imageFlags & nvg::NVG_IMAGE_REPEATY)
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT);
    else
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);

    gl::PixelStorei(gl::UNPACK_ALIGNMENT, 4);
    gl::PixelStorei(gl::UNPACK_ROW_LENGTH, 0);
    gl::PixelStorei(gl::UNPACK_SKIP_PIXELS, 0);
    gl::PixelStorei(gl::UNPACK_SKIP_ROWS, 0);

    if (imageFlags & nvg::NVG_IMAGE_GENERATE_MIPMAPS)
    {
        gl::GenerateMipmap(gl::TEXTURE_2D);
    }

    CheckError("create tex");
    BindTexture(0);

    return tex->id;
}

int glnvg::Context::UpdateTexture(int image, int x, int y, int w, int h, const u8* data)
{
	const auto tex = FindTexture(image);

	if (!tex) return 0;
	BindTexture(tex->tex);
	
	gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);

	gl::PixelStorei(gl::UNPACK_ROW_LENGTH, tex->width);
	gl::PixelStorei(gl::UNPACK_SKIP_PIXELS, x);
	gl::PixelStorei(gl::UNPACK_SKIP_ROWS, y);

	if (tex->type == nvg::NVG_TEXTURE_RGBA)
		gl::TexSubImage2D(gl::TEXTURE_2D, 0, x, y, w, h, gl::RGBA, gl::UNSIGNED_BYTE, data);
	else
		gl::TexSubImage2D(gl::TEXTURE_2D, 0, x, y, w, h, gl::RED, gl::UNSIGNED_BYTE, data);

	gl::PixelStorei(gl::UNPACK_ALIGNMENT, 4);
	gl::PixelStorei(gl::UNPACK_ROW_LENGTH, 0);
	gl::PixelStorei(gl::UNPACK_SKIP_PIXELS, 0);
	gl::PixelStorei(gl::UNPACK_SKIP_ROWS, 0);

	BindTexture(0);
	
	return 1;
}

std::optional<Extent> glnvg::Context::GetTextureSize(const int image)
{
	const auto tex = FindTexture(image);
	if (!tex) return {};
	return Extent(tex->width, tex->height);
}

void xformToMat3x4(float* m3, float* t)
{
	m3[0] = t[0];
	m3[1] = t[1];
	m3[2] = 0.0f;
	m3[3] = 0.0f;
	m3[4] = t[2];
	m3[5] = t[3];
	m3[6] = 0.0f;
	m3[7] = 0.0f;
	m3[8] = t[4];
	m3[9] = t[5];
	m3[10] = 1.0f;
	m3[11] = 0.0f;
}

nvg::color premulColor(nvg::color c)
{
	c.r *= c.a;
	c.g *= c.a;
	c.b *= c.a;
	return c;
}

int glnvg::Context::ConvertPaint(FragUniforms* frag, const nvg::paint& paint, const nvg::scissor& scissor, float width, float fringe, float strokeThr)
{
	float invxform[6];

	memset(frag, 0, sizeof(*frag));

	frag->innerCol = premulColor(paint.innerColor);
	frag->outerCol = premulColor(paint.outerColor);

	if (scissor.extent[0] < -0.5f || scissor.extent[1] < -0.5f) {
		memset(frag->scissorMat, 0, sizeof(frag->scissorMat));
		frag->scissorExt[0] = 1.0f;
		frag->scissorExt[1] = 1.0f;
		frag->scissorScale[0] = 1.0f;
		frag->scissorScale[1] = 1.0f;
	}
	else {
		nvg::TransformInverse(invxform, scissor.xform);
		xformToMat3x4(frag->scissorMat, invxform);
		frag->scissorExt[0] = scissor.extent[0];
		frag->scissorExt[1] = scissor.extent[1];
		frag->scissorScale[0] = sqrtf(scissor.xform[0] * scissor.xform[0] + scissor.xform[2] * scissor.xform[2]) / fringe;
		frag->scissorScale[1] = sqrtf(scissor.xform[1] * scissor.xform[1] + scissor.xform[3] * scissor.xform[3]) / fringe;
	}

	memcpy(frag->extent, paint.extent, sizeof(frag->extent));
	frag->strokeMult = (width*0.5f + fringe*0.5f) / fringe;
	frag->strokeThr = strokeThr;

	if (paint.image != 0) 
	{
		const auto tex = FindTexture(paint.image);
		if (!tex) return 0;
		if ((tex->flags & nvg::NVG_IMAGE_FLIPY) != 0) 
		{
			float m1[6], m2[6];
			nvg::TransformTranslate(m1, 0.0f, frag->extent[1] * 0.5f);
			nvg::TransformMultiply(m1, paint.xform);
			nvg::TransformScale(m2, 1.0f, -1.0f);
			nvg::TransformMultiply(m2, m1);
			nvg::TransformTranslate(m1, 0.0f, -frag->extent[1] * 0.5f);
			nvg::TransformMultiply(m1, m2);
			nvg::TransformInverse(invxform, m1);
		}
		else 
		{
			nvg::TransformInverse(invxform, paint.xform);
		}
		frag->type = ShaderType::FILLIMG;

		if (tex->type == nvg::NVG_TEXTURE_RGBA)
			frag->texType = (tex->flags & nvg::NVG_IMAGE_PREMULTIPLIED) ? 0 : 1;
		else
			frag->texType = 2;
	}
	else 
	{
		frag->type = ShaderType::FILLGRAD;
		frag->radius = paint.radius;
		frag->feather = paint.feather;
		nvg::TransformInverse(invxform, paint.xform);
	}

	xformToMat3x4(frag->paintMat, invxform);

	return 1;
}

void glnvg::Context::SetUniforms(int uniformOffset, int image)
{
	gl::BindBufferRange(gl::UNIFORM_BUFFER, FRAG_BINDING, fragBuf, uniformOffset, sizeof(glnvg::FragUniforms));

	if (image != 0) {
		glnvg::Texture* tex = FindTexture(image);
		BindTexture(tex ? tex->tex : 0);
		CheckError("tex paint tex");
	}
	else 
	{
		BindTexture(0);
	}
}

void glnvg::Context::SetViewport(const int width, const int height)
{
	view[0] = static_cast<float>(width);
	view[1] = static_cast<float>(height);
}

void glnvg::Context::Fill(const Call& call)
{
	const auto paths = &m_paths[call.pathOffset];
	int npaths = call.pathCount;

	// Draw shapes
	gl::Enable(gl::STENCIL_TEST);
	StencilMask(0xff);
	StencilFunc(gl::ALWAYS, 0, 0xff);
	gl::ColorMask(gl::FALSE_, gl::FALSE_, gl::FALSE_, gl::FALSE_);

	// set bindpoint for solid loc
	SetUniforms(call.uniformOffset, 0);
	CheckError("fill simple");

	gl::StencilOpSeparate(gl::FRONT, gl::KEEP, gl::KEEP, gl::INCR_WRAP);
	gl::StencilOpSeparate(gl::BACK, gl::KEEP, gl::KEEP, gl::DECR_WRAP);
	gl::Disable(gl::CULL_FACE);
	for (int i = 0; i < npaths; i++)
		gl::DrawArrays(gl::TRIANGLE_FAN, paths[i].fillOffset, paths[i].fillCount);
	gl::Enable(gl::CULL_FACE);

	// Draw anti-aliased pixels
	gl::ColorMask(gl::TRUE_, gl::TRUE_, gl::TRUE_, gl::TRUE_);

	SetUniforms(call.uniformOffset + fragSize, call.image);
	CheckError("fill fill");

	if (flags & nvg::ANTIALIAS) 
	{
		StencilFunc(gl::EQUAL, 0x00, 0xff);
		gl::StencilOp(gl::KEEP, gl::KEEP, gl::KEEP);
		// Draw fringes
		for (int i = 0; i < npaths; i++)
			gl::DrawArrays(gl::TRIANGLE_STRIP, paths[i].strokeOffset, paths[i].strokeCount);
	}

	// Draw fill
	StencilFunc(gl::NOTEQUAL, 0x0, 0xff);
	gl::StencilOp(gl::ZERO, gl::ZERO, gl::ZERO);
	gl::DrawArrays(gl::TRIANGLE_STRIP, call.triangleOffset, call.triangleCount);

	gl::Disable(gl::STENCIL_TEST);
}

void glnvg::Context::ConvexFill(const Call& call)
{
	const auto paths = &m_paths[call.pathOffset];
	int npaths = call.pathCount;

	SetUniforms(call.uniformOffset, call.image);
	CheckError("convex fill");

	for (int i = 0; i < npaths; i++)
		gl::DrawArrays(gl::TRIANGLE_FAN, paths[i].fillOffset, paths[i].fillCount);
	if (flags & nvg::ANTIALIAS) 
	{
		// Draw fringes
		for (int i = 0; i < npaths; i++)
			gl::DrawArrays(gl::TRIANGLE_STRIP, paths[i].strokeOffset, paths[i].strokeCount);
	}
}

void glnvg::Context::Stroke(const Call& call)
{
	const auto paths = &m_paths[call.pathOffset];
	int npaths = call.pathCount;

	if (flags & nvg::STENCIL_STROKES) {

		gl::Enable(gl::STENCIL_TEST);
		StencilMask(0xff);

		// Fill the stroke base without overlap
		StencilFunc(gl::EQUAL, 0x0, 0xff);
		gl::StencilOp(gl::KEEP, gl::KEEP, gl::INCR);
		SetUniforms(call.uniformOffset + fragSize, call.image);
		CheckError("stroke fill 0");
		for (int i = 0; i < npaths; i++)
			gl::DrawArrays(gl::TRIANGLE_STRIP, paths[i].strokeOffset, paths[i].strokeCount);

		// Draw anti-aliased pixels.
		SetUniforms(call.uniformOffset, call.image);
		StencilFunc(gl::EQUAL, 0x00, 0xff);
		gl::StencilOp(gl::KEEP, gl::KEEP, gl::KEEP);
		for (int i = 0; i < npaths; i++)
			gl::DrawArrays(gl::TRIANGLE_STRIP, paths[i].strokeOffset, paths[i].strokeCount);

		// Clear stencil buffer.
		gl::ColorMask(gl::FALSE_, gl::FALSE_, gl::FALSE_, gl::FALSE_);
		StencilFunc(gl::ALWAYS, 0x0, 0xff);
		gl::StencilOp(gl::ZERO, gl::ZERO, gl::ZERO);
		CheckError("stroke fill 1");
		for (int i = 0; i < npaths; i++)
			gl::DrawArrays(gl::TRIANGLE_STRIP, paths[i].strokeOffset, paths[i].strokeCount);
		gl::ColorMask(gl::TRUE_, gl::TRUE_, gl::TRUE_, gl::TRUE_);

		gl::Disable(gl::STENCIL_TEST);
	}
	else 
    {
		SetUniforms(call.uniformOffset, call.image);
		CheckError("stroke fill");
		// Draw Strokes
		for (int i = 0; i < npaths; i++)
			gl::DrawArrays(gl::TRIANGLE_STRIP, paths[i].strokeOffset, paths[i].strokeCount);
	}
}

void glnvg::Context::Triangles(const Call& call)
{
	SetUniforms(call.uniformOffset, call.image);
	CheckError("triangles fill");

	gl::DrawArrays(gl::TRIANGLES, call.triangleOffset, call.triangleCount);
}

void glnvg::Context::Cancel() 
{
    m_nverts = 0;
    m_numPaths = 0;
    m_numCalls = 0;
	nuniforms = 0;
}

GLenum convertBlendFuncFactor(int factor)
{
	if (factor == nvg::NVG_ZERO)
		return gl::ZERO;
	if (factor == nvg::NVG_ONE)
		return gl::ONE;
	if (factor == nvg::NVG_SRC_COLOR)
		return gl::SRC_COLOR;
	if (factor == nvg::NVG_ONE_MINUS_SRC_COLOR)
		return gl::ONE_MINUS_SRC_COLOR;
	if (factor == nvg::NVG_DST_COLOR)
		return gl::DST_COLOR;
	if (factor == nvg::NVG_ONE_MINUS_DST_COLOR)
		return gl::ONE_MINUS_DST_COLOR;
	if (factor == nvg::NVG_SRC_ALPHA)
		return gl::SRC_ALPHA;
	if (factor == nvg::NVG_ONE_MINUS_SRC_ALPHA)
		return gl::ONE_MINUS_SRC_ALPHA;
	if (factor == nvg::NVG_DST_ALPHA)
		return gl::DST_ALPHA;
	if (factor == nvg::NVG_ONE_MINUS_DST_ALPHA)
		return gl::ONE_MINUS_DST_ALPHA;
	if (factor == nvg::NVG_SRC_ALPHA_SATURATE)
		return gl::SRC_ALPHA_SATURATE;
	return gl::INVALID_ENUM;
}

glnvg::Blend blendCompositeOperation(nvg::CompositeOperationState op)
{
	glnvg::Blend blend;
	blend.srcRGB = convertBlendFuncFactor(op.srcRGB);
	blend.dstRGB = convertBlendFuncFactor(op.dstRGB);
	blend.srcAlpha = convertBlendFuncFactor(op.srcAlpha);
	blend.dstAlpha = convertBlendFuncFactor(op.dstAlpha);
	if (blend.srcRGB == gl::INVALID_ENUM || blend.dstRGB == gl::INVALID_ENUM || blend.srcAlpha == gl::INVALID_ENUM || blend.dstAlpha == gl::INVALID_ENUM)
	{
		blend.srcRGB = gl::ONE;
		blend.dstRGB = gl::ONE_MINUS_SRC_ALPHA;
		blend.srcAlpha = gl::ONE;
		blend.dstAlpha = gl::ONE_MINUS_SRC_ALPHA;
	}
	return blend;
}

void glnvg::Context::Flush()
{
	if (m_numCalls == 0)
	{
        Cancel();
        return;
	}

    // Setup require GL state.
    gl::UseProgram(shader.m_shader);

    gl::Enable(gl::CULL_FACE);
    gl::CullFace(gl::BACK);
    gl::FrontFace(gl::CCW);
    gl::Enable(gl::BLEND);
    gl::Disable(gl::DEPTH_TEST);
    gl::Disable(gl::SCISSOR_TEST);
    gl::ColorMask(gl::TRUE_, gl::TRUE_, gl::TRUE_, gl::TRUE_);
    gl::StencilMask(0xffffffff);
    gl::StencilOp(gl::KEEP, gl::KEEP, gl::KEEP);
    gl::StencilFunc(gl::ALWAYS, 0, 0xffffffff);
    gl::ActiveTexture(gl::TEXTURE0);
    gl::BindTexture(gl::TEXTURE_2D, 0);
    m_boundTexture = 0;
    m_stencilMask = 0xffffffff;
    m_stencilFunc = gl::ALWAYS;
    m_stencilFuncRef = 0;
    m_stencilFuncMask = 0xffffffff;
    m_blendFunc.srcRGB = gl::INVALID_ENUM;
    m_blendFunc.srcAlpha = gl::INVALID_ENUM;
    m_blendFunc.dstRGB = gl::INVALID_ENUM;
    m_blendFunc.dstAlpha = gl::INVALID_ENUM;

    // Upload ubo for frag shaders
    gl::BindBuffer(gl::UNIFORM_BUFFER, fragBuf);
    gl::BufferData(gl::UNIFORM_BUFFER, nuniforms * fragSize, uniforms, gl::STREAM_DRAW);

    // Upload vertex data
    gl::BindVertexArray(vertArr);
    gl::BindBuffer(gl::ARRAY_BUFFER, vertBuf);
    gl::BufferData(gl::ARRAY_BUFFER, m_nverts * sizeof(nvg::vertex), m_verts, gl::STREAM_DRAW);
    gl::EnableVertexAttribArray(0);
    gl::EnableVertexAttribArray(1);
    gl::VertexAttribPointer(0, 2, gl::FLOAT, gl::FALSE_, sizeof(nvg::vertex), (const GLvoid*)(size_t)0);
    gl::VertexAttribPointer(1, 2, gl::FLOAT, gl::FALSE_, sizeof(nvg::vertex), (const GLvoid*)(0 + 2 * sizeof(float)));

    // Set view and texture just once per frame.
    gl::Uniform1i(shader.m_loc[LOC_TEX], 0);
    gl::Uniform2fv(shader.m_loc[LOC_VIEWSIZE], 1, view);

    gl::BindBuffer(gl::UNIFORM_BUFFER, fragBuf);

    for (size_t i = 0; i < m_numCalls; ++i)
    {
        const auto& call = m_calls[i];
        BlendFuncSeparate(call.blendFunc);
        switch (call.type)
        {
        case Call::Type::FILL:
            Fill(call);
            break;
        case Call::Type::CONVEXFILL:
            ConvexFill(call);
            break;
        case Call::Type::STROKE:
            Stroke(call);
            break;
        case Call::Type::TRIANGLES:
            Triangles(call);
            break;
        }
    }

    gl::DisableVertexAttribArray(0);
    gl::DisableVertexAttribArray(1);
    gl::BindVertexArray(0);
    gl::Disable(gl::CULL_FACE);
    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
    gl::UseProgram(0);
    BindTexture(0);

    Cancel();
}

int maxVertCount(const nvg::Path* paths, int npaths)
{
	int count = 0;
	for (int i = 0; i < npaths; i++) 
	{
		count += paths[i].nfill;
		count += paths[i].nstroke;
	}
	return count;
}

glnvg::Call& glnvg::Context::AllocCall()
{
	if (m_numCalls + 1 > m_calls.size())
        m_calls.push_back(Call());
	
    return m_calls[m_numCalls++];
}

int glnvg::Context::AllocPaths(int n)
{
    if (m_numPaths + n > m_paths.size())
        for (size_t i = 0; i < n; ++i)
            m_paths.push_back(Path());

    m_numPaths += n;
    return m_numPaths - n;
}

int glnvg::Context::AllocVerts(int n)
{
	if (m_nverts + n > m_cverts)
	{
		int cverts = maxi(m_nverts + n, 4096) + this->m_cverts / 2; // 1.5x Overallocate
		nvg::vertex* verts = (nvg::vertex*)realloc(this->m_verts, sizeof(nvg::vertex) * cverts);
		if (!verts) return -1;
		this->m_verts = verts;
		this->m_cverts = cverts;
	}

	int ret = m_nverts;
    m_nverts += n;
	return ret;
}

int glnvg::Context::AllocFragUniforms(int n)
{
	int structSize = fragSize;
	if (nuniforms + n > cuniforms) 
	{
		int cuniforms = maxi(nuniforms + n, 128) + this->cuniforms / 2; // 1.5x Overallocate
		unsigned char* uniforms = (unsigned char*)realloc(this->uniforms, structSize * cuniforms);
		if (!uniforms) return -1;
		this->uniforms = uniforms;
		this->cuniforms = cuniforms;
	}
	int ret = nuniforms * structSize;
	nuniforms += n;
	return ret;
}

glnvg::FragUniforms* glnvg::Context::FragUniformPtr(int i)
{
	return (glnvg::FragUniforms*)&uniforms[i];
}

void vset(nvg::vertex* vtx, float x, float y, float u, float v)
{
	vtx->x = x;
	vtx->y = y;
	vtx->u = u;
	vtx->v = v;
}

void glnvg::Context::RenderFill(nvg::CompositeOperationState compositeOperation, const nvg::paint& paint, const nvg::scissor& scissor, float fringe, const float* bounds, const nvg::Path* paths, int npaths)
{
	auto& call = AllocCall();
	
	call.type = Call::Type::FILL;
	call.triangleCount = 4;
	call.pathOffset = AllocPaths(npaths);
	if (call.pathOffset == -1) goto error;
	call.pathCount = npaths;
	call.image = paint.image;
	call.blendFunc = blendCompositeOperation(compositeOperation);

	if (npaths == 1 && paths[0].convex)
	{
		call.type = Call::Type::CONVEXFILL;
		call.triangleCount = 0;	// Bounding box fill quad not needed for convex fill
	}

	// Allocate vertices for all the paths.
	const int maxverts = maxVertCount(paths, npaths) + call.triangleCount;
	int offset = AllocVerts(maxverts);
	if (offset == -1) goto error;

	for (int i = 0; i < npaths; i++) 
	{
		auto copy = &m_paths[call.pathOffset + i];
		const auto path = &paths[i];
		memset(copy, 0, sizeof(*copy));
		if (path->nfill > 0) 
        {
			copy->fillOffset = offset;
			copy->fillCount = path->nfill;
			memcpy(&m_verts[offset], path->fill, sizeof(nvg::vertex) * path->nfill);
			offset += path->nfill;
		}
		if (path->nstroke > 0) 
        {
			copy->strokeOffset = offset;
			copy->strokeCount = path->nstroke;
			memcpy(&m_verts[offset], path->stroke, sizeof(nvg::vertex) * path->nstroke);
			offset += path->nstroke;
		}
	}

	// Setup uniforms for draw calls
	if (call.type == Call::Type::FILL) 
	{
		// Quad
		call.triangleOffset = offset;
		const auto quad = &m_verts[call.triangleOffset];
		vset(&quad[0], bounds[2], bounds[3], 0.5f, 1.0f);
		vset(&quad[1], bounds[2], bounds[1], 0.5f, 1.0f);
		vset(&quad[2], bounds[0], bounds[3], 0.5f, 1.0f);
		vset(&quad[3], bounds[0], bounds[1], 0.5f, 1.0f);

		call.uniformOffset = AllocFragUniforms(2);
		if (call.uniformOffset == -1) goto error;
		// Simple shader for stencil
		const auto frag = FragUniformPtr(call.uniformOffset);
		memset(frag, 0, sizeof(*frag));
		frag->strokeThr = -1.0f;
		frag->type = ShaderType::SIMPLE;
		// Fill shader
		ConvertPaint(FragUniformPtr(call.uniformOffset + fragSize), paint, scissor, fringe, fringe, -1.0f);
	}
	else 
	{
		call.uniformOffset = AllocFragUniforms(1);
		if (call.uniformOffset == -1) goto error;
		// Fill shader
		ConvertPaint(FragUniformPtr(call.uniformOffset), paint, scissor, fringe, fringe, -1.0f);
	}

	return;

error:
	// We get here if call alloc was ok, but something else is not.
	// Roll back the last call to prevent drawing it.
	if (m_numCalls > 0) m_numCalls--;
}

void glnvg::Context::RenderStroke(nvg::CompositeOperationState compositeOperation, const nvg::paint& paint, const nvg::scissor& scissor, float fringe, float strokeWidth, const nvg::Path* paths, int npaths)
{
	auto& call = AllocCall();
	
	call.type = Call::Type::STROKE;
	call.pathOffset = AllocPaths(npaths);
	if (call.pathOffset == -1) goto error;
	call.pathCount = npaths;
	call.image = paint.image;
	call.blendFunc = blendCompositeOperation(compositeOperation);

	// Allocate vertices for all the paths.
	const int maxverts = maxVertCount(paths, npaths);
	int offset = AllocVerts(maxverts);
	if (offset == -1) goto error;

	for (int i = 0; i < npaths; i++) 
	{
		const auto copy = &m_paths[call.pathOffset + i];
		const auto path = &paths[i];
		memset(copy, 0, sizeof(*copy));
		if (path->nstroke) 
        {
			copy->strokeOffset = offset;
			copy->strokeCount = path->nstroke;
			memcpy(&m_verts[offset], path->stroke, sizeof(nvg::vertex) * path->nstroke);
			offset += path->nstroke;
		}
	}

	if (flags & nvg::STENCIL_STROKES) 
	{
		// Fill shader
		call.uniformOffset = AllocFragUniforms(2);
		if (call.uniformOffset == -1) goto error;

		ConvertPaint(FragUniformPtr(call.uniformOffset), paint, scissor, strokeWidth, fringe, -1.0f);
		ConvertPaint(FragUniformPtr(call.uniformOffset + fragSize), paint, scissor, strokeWidth, fringe, 1.0f - 0.5f / 255.0f);

	}
	else 
	{
		// Fill shader
		call.uniformOffset = AllocFragUniforms(1);
		if (call.uniformOffset == -1) goto error;
		ConvertPaint(FragUniformPtr(call.uniformOffset), paint, scissor, strokeWidth, fringe, -1.0f);
	}

	return;

error:
	// We get here if call alloc was ok, but something else is not.
	// Roll back the last call to prevent drawing it.
	if (m_numCalls > 0) m_numCalls--;
}

void glnvg::Context::RenderTriangles(nvg::CompositeOperationState compositeOperation, const nvg::paint& paint, const nvg::scissor& scissor, const nvg::vertex* verts, int nverts)
{
	auto& call = AllocCall();
	
	call.type = Call::Type::TRIANGLES;
	call.image = paint.image;
	call.blendFunc = blendCompositeOperation(compositeOperation);

	// Allocate vertices for all the paths.
	call.triangleOffset = AllocVerts(nverts);
	if (call.triangleOffset == -1)
	{
        if (m_numCalls > 0) m_numCalls--;
        return;
	}
	call.triangleCount = nverts;

	memcpy(&m_verts[call.triangleOffset], verts, sizeof(nvg::vertex) * nverts);

	// Fill shader
	call.uniformOffset = AllocFragUniforms(1);
	if (call.uniformOffset == -1)
    {
        if (m_numCalls > 0) m_numCalls--;
        return;
    }
	const auto frag = FragUniformPtr(call.uniformOffset);
	ConvertPaint(frag, paint, scissor, 1.0f, 1.0f, -1.0f);
	frag->type = ShaderType::IMG;
}

glnvg::Context::~Context()
{
	shader.Delete();
	
	if (fragBuf)
		gl::DeleteBuffers(1, &fragBuf);
	if (vertArr)
		gl::DeleteVertexArrays(1, &vertArr);
	if (vertBuf)
		gl::DeleteBuffers(1, &vertBuf);

	for (auto& texture : textures)
		if (texture.tex && (texture.flags & nvg::IMAGE_NODELETE) == 0)
			gl::DeleteTextures(1, &texture.tex);
	
	free(m_verts);
	free(uniforms);
}

int glnvg::CreateImageFromHandle(nvg::Context* ctx, const GLuint textureId, const int w, const int h, const int imageFlags)
{
	const auto tex = ctx->glContext->AllocTexture();

	if (!tex) return 0;

	tex->type = nvg::NVG_TEXTURE_RGBA;
	tex->tex = textureId;
	tex->flags = imageFlags;
	tex->width = w;
	tex->height = h;

	return tex->id;
}

GLuint glnvg::GetImageHandle(nvg::Context* ctx, const int image)
{
	return ctx->glContext->FindTexture(image)->tex;
}
