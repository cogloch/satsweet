#pragma once
#include <vis.h>
#include <array>
#include <vector>
#include <string>
#include <optional>

#define FONS_INVALID -1
#undef DrawText

// Based on Exponential blur, Jani Huhtanen, 2006
void BlurCols(u8* dst, int w, int h, int dstStride, int alpha);
void BlurRows(u8* dst, int w, int h, int dstStride, int alpha);
void Blur(u8* dst, int w, int h, int dstStride, int blur);


namespace fons
{
	struct TextMetrics
	{
		float ascender;
		float descender;
		float lineh;
		void UniformScale(const float scale);
	};

	enum Flags
	{
		ZERO_TOPLEFT = 1,
		ZERO_BOTTOMLEFT = 2,
	};

	enum Align 
	{
		// Horizontal align
		LEFT 	 = 1<<0,	// Default
		CENTER 	 = 1<<1,
		RIGHT 	 = 1<<2,
		// Vertical align
		TOP 	 = 1<<3,
		MIDDLE	 = 1<<4,
		BOTTOM	 = 1<<5,
		BASELINE = 1<<6, // Default
	};

	enum ErrorCode 
	{
		// Font atlas is full.
		ATLAS_FULL = 1,
		// Scratch memory used to render glyphs is full, requested size reported in 'val', you may need to bump up FONS_SCRATCH_BUF_SIZE.
		SCRATCH_FULL = 2,
		// Calls to fonsPushState has created too large stack, if you need deep state stack bump up FONS_MAX_STATES.
		STATES_OVERFLOW = 3,
		// Trying to pop too many states fonsPopState().
		STATES_UNDERFLOW = 4,
	};

	struct Params 
	{
		Params(const int width = 0, const int height = 0, const u8 flags = 0);

		int width, height;
		u8 flags;
		void* userPtr;
		int (*renderCreate)(void* uptr, int width, int height);
		int (*renderResize)(void* uptr, int width, int height);
		void (*renderUpdate)(void* uptr, int* rect, const u8* data);
		void (*renderDraw)(void* uptr, const float* verts, const float* tcoords, const unsigned int* colors, int nverts);
		void (*renderDelete)(void* uptr);
	};

	struct Quad
	{
		float x0,y0,s0,t0;
		float x1,y1,s1,t1;
	};

	struct TextIter 
	{
		float x, y, nextx, nexty, scale, spacing;
		unsigned int codepoint;
		short isize, iblur;
		struct Font* font;
		int prevGlyphIndex;
		const char* str;
		const char* next;
		const char* end;
		unsigned int utf8state;
	};

	#define FONS_NOTUSED(v)  (void)sizeof(v)

	const int SCRATCH_BUF_SIZE = 64000;
	const int HASH_LUT_SIZE = 256;
	const int INIT_FONTS = 4;
	const int INIT_GLYPHS = 256;
	const int INIT_ATLAS_NODES = 256;
	const int VERTEX_COUNT = 1024;
	const int MAX_STATES = 20;
	const int MAX_FALLBACKS = 20;

	unsigned int hashint(unsigned int a);
	int mini(int a, int b);
	int maxi(int a, int b);

	struct Glyph
	{
		unsigned int codepoint;
		int index;
		int next;
		short size, blur;
		short x0,y0,x1,y1;
		short xadv,xoff,yoff;
	};

	struct Atlas
	{
		Atlas(const int w, const int h, const int nnodes);
		~Atlas();

		int width, height;
		struct Node
		{
			Node();
			short x, y, width;
		} *nodes;
		int nnodes;
		int cnodes;

		int InsertNode(int idx, int x, int y, int w);
		void RemoveNode(int idx);
		void Expand(int w, int h);
		void Reset(int w, int h);
		int AddSkylineLevel(int idx, int x, int y, int w, int h);
		int RectFits(int i, int w, int h);
		int AddRect(int rw, int rh, int* rx, int* ry);
	};

	struct Font;
	Glyph* AllocGlyph(Font* font);

	struct State
	{
		int font;
		int align;
		float size;
		unsigned int color;
		float blur;
		float spacing;
	};

	struct Context
	{
		Context(const Params params);
		~Context();

		Params params;
		float itw,ith;
		u8* texData;
		int dirtyRect[4];
		std::vector<Font*> fonts;
		Atlas* atlas;
		float verts[VERTEX_COUNT*2];
		float tcoords[VERTEX_COUNT*2];
		unsigned int colors[VERTEX_COUNT];
		int nverts;
		u8* scratch;
		int nscratch;
		State states[MAX_STATES];
		int nstates;
		void (*handleError)(void* uptr, int error, int val);
		void* errorUptr;

		void SetErrorCallback(void(*callback)(void* uptr, int error, int val), void* uptr);
		// Returns current atlas size.
		void GetAtlasSize(int* width, int* height);
		// Expands the atlas size.
		int ExpandAtlas(int width, int height);
		// Resets the whole stash.
		int ResetAtlas(int width, int height);

		int GetFontByName(const std::string& name) const;

		// State handling
		void PushState();
		void PopState();
		void ClearState();

		// State setting
		void SetSize(float size);
		void SetColor(unsigned int color);
		void SetSpacing(float spacing);
		void SetBlur(float blur);
		void SetAlign(int align);
		void SetFont(int font);

		// Draw text
		float DrawText(float x, float y, const char* string, const char* end);

		// Measure text
		float TextBounds(float x, float y, const char* string, const char* end, float* bounds);
		void LineBounds(float y, float* miny, float* maxy);
		TextMetrics VertMetrics();

		// Text iterator
		std::optional<TextIter> TextIterInit(float x, float y, const char* str, const char* end);
		bool TextIterNext(TextIter* iter, Quad* quad);

		// Pull texture changes
		const u8* GetTextureData(int* width, int* height);
		int ValidateTexture(int* dirty);

		// Draws the stash texture for debugging
		void DrawDebug(float x, float y);

		void Flush();

		void Vertex(float x, float y, float s, float t, unsigned int c);

		State* GetState();

		void AddWhiteRect(int w, int h);
		Glyph* GetGlyph(Font* font, unsigned int codepoint, short isize, short iblur);
		void GetQuad(Font* font, int prevGlyphIndex, Glyph* glyph, float scale, float spacing, float* x, float* y, Quad* q);
		float GetVertAlign(Font* font, int align, short isize);
	};
}


