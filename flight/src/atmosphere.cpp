#include "stdafx.h"
#include "atmosphere.h"


namespace atmo
{
	std::optional<State> FLY_API GetStateAtAltitude(double alt)
	{
		if (alt < 0.0 || alt > 2000.0)
			return {};

		// Find index of the layer [alt] is in 
		size_t layerIdx = 0;
		for (const size_t numLayers = layers.size() - 1; layerIdx < numLayers; ++layerIdx)
		{
			if (layers[layerIdx + 1].h > alt)
				break;
		}

		State result;
		return result;
	}
}
