#version 330 core
out vec4 FragColor;

in vec2 TexCoords;
in vec3 Normal;
in vec3 FragPos;  

//uniform sampler2D texture_diffuse1;

void main()
{    
    vec3 lightColor = vec3(1.0, 1.0, 1.0);
    vec3 lightPos = vec3(1.2, 1.0, 2.0);
    vec3 objectColor = vec3(1.0, 0.5, 0.0);

    float ambientStrength = 0.9;
    vec3 ambient = ambientStrength * lightColor;

    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);  

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    vec3 result = (ambient + diffuse) * objectColor;
    FragColor = vec4(result, 1.0);

    //FragColor = texture(texture_diffuse1, TexCoords);
    //FragColor = vec4(objectColor, 1.0);
}