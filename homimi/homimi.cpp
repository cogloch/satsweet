#include "stdafx.h"
#include "flight\atmosphere.h"
#include "vis\vis_window.h"

//#pragma comment(lib, "flight.lib")
//#pragma comment(lib, "vis.lib")

#include "vis\vis_resources.h"


int main()
{
	VisWindow window;

	// Load viz resources 
	vis::fonts::LoadFromFile("assets/UbuntuMono-R.ttf", "sans");
	vis::fonts::LoadFromFile("assets/UbuntuMono-B.ttf", "sans-bold");

	vis::meshes::LoadMesh("assets/can.obj");

	// Cache viz resources 
	window.CopyResources();

	window.Loop();
}

